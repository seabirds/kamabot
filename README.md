# Kama-Bot

## Test en local

Lancer simplement l'application Spring Boot

## Déploiement

Il vous faut un environment Docker pour construire le container

Faire un maven package :
`mvn package`

Puis utiliser le Dockerfile pour construire et déployer l'image du container sur la plateforme de votre choix
