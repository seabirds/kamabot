package com.ethernity.kamabot.configuration;

import com.ethernity.kamabot.command.Command;
import com.ethernity.kamabot.command.CommandRegistry;
import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.slashcommand.SlashCommand;
import com.ethernity.kamabot.slashcommand.SlashCommandRegistry;
import com.ethernity.kamabot.watcher.Watcher;
import com.ethernity.kamabot.watcher.WatcherRegistry;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Configuration
@Slf4j
public class KamaBotConfiguration
{
  @Autowired
  private DiscordIDs discordIDs;

  @Bean
  public CommandRegistry commandRegistry(List<Command> commands, DiscordIDs discordIDs)
  {
    CommandRegistry commandRegistry = new CommandRegistry(discordIDs);
    commands.forEach(commandRegistry::register);
    return commandRegistry;
  }

  @Bean
  public DiscordApi api()
  { // Returns a new instance of the Discord client
    DiscordApiBuilder api = new DiscordApiBuilder()
        .setAllIntents()
        .setUserCacheEnabled(true)
        .setToken(discordIDs.token); // Creates the ClientBuilder instance
    try {
      DiscordApi client = api.login().join(); // Creates the client instance and logs the client in
      return client;
    } catch (Exception e) { // This is thrown if there was a problem building the client
      log.error("Erreur d'initialisation du client Discord", e);
      throw e;
    }
  }

  @Bean
  public SlashCommandRegistry slashCommandRegistry(List<SlashCommand> commands, DiscordService discordService, DiscordIDs discordIDs)
  {
    SlashCommandRegistry commandRegistry = new SlashCommandRegistry(discordService, discordIDs);
    commands.forEach(commandRegistry::register);
    return commandRegistry;
  }

  @Bean
  public WatcherRegistry watcherRegistry(List<Watcher> watchers, DiscordService discordService, DiscordIDs discordIDs)
  {
    WatcherRegistry watcherRegistry = new WatcherRegistry(discordService, discordIDs);
    watchers.forEach(watcherRegistry::register);
    return watcherRegistry;
  }

  @Bean
  public FirebaseOptions firebaseOptionsConfiguration() throws IOException
  {
    InputStream serviceAccount =
        getClass().getClassLoader().getResourceAsStream("service-account-key.json");

    return new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl("https://kama-bot-default-rtdb.europe-west1.firebasedatabase.app")
        .build();
  }

  @Bean
  public FirebaseApp firebaseAppConfiguration(FirebaseOptions options)
  {
    return FirebaseApp.initializeApp(options);
  }

  @Bean
  public FirebaseAuth firebaseAuthConfiguration(FirebaseApp app) throws IOException
  {
    return FirebaseAuth.getInstance(app);
  }

  @Bean
  public FirebaseDatabase firebaseConfiguration(FirebaseApp app) throws IOException
  {
    // The app only has access as defined in the Security Rules
    return FirebaseDatabase.getInstance(app);
  }
}
