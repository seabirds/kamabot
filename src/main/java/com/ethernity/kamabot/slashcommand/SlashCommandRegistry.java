package com.ethernity.kamabot.slashcommand;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.interaction.MessageContextMenuInteraction;
import org.javacord.api.interaction.ModalInteraction;
import org.javacord.api.interaction.SlashCommandInteraction;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
public class SlashCommandRegistry
{

  private final DiscordService discordService;
  private final DiscordIDs discordIDs;

  private final Map<String, SlashCommand> commands = new HashMap<>();
  private final Map<String, SlashCommand> contextMenuCommands = new HashMap<>();

  public SlashCommandRegistry(DiscordService discordService, DiscordIDs discordIDs)
  {
    this.discordService = discordService;
    this.discordIDs = discordIDs;
  }

  public void register(SlashCommand cmd)
  {
    cmd.setup();
    commands.put(cmd.getCommandName(), cmd);
    for (String contextMenuCommandName : cmd.getMessageContextMenuCommands()) {
      contextMenuCommands.put(contextMenuCommandName, cmd);
    }

  }

  public void execute(String channelId, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException
  {
    SlashCommand command = commands.get(slashCommandInteraction.getCommandName());
    if (command != null) {
      log.info("Slash command " + command.getCommandName() + " detected by " + slashCommandInteraction.getUser().getDisplayName(slashCommandInteraction.getServer().get()));
      command.execute(channelId, slashCommandInteraction);
    } else {
      log.info("Command unrecognized, ignoring");
    }
  }

  public void executeModal(String channelId, ModalInteraction modalInteraction) throws ExecutionException, InterruptedException
  {
    String[] explodedCustomId = modalInteraction.getCustomId().split("//");
    SlashCommand command = commands.get(explodedCustomId[0]);
    if (command != null) {
      log.info("Modal submission for slash command " + command.getCommandName() + " detected by " + modalInteraction.getUser().getName());
      command.executeModal(channelId, modalInteraction);
    } else {
      log.info("Modal submission by " + modalInteraction.getUser().getName() + " unrecognized, ignoring");
    }
  }

  public void executeContextMenu(String channelId, MessageContextMenuInteraction messageContextMenuInteraction) throws ExecutionException, InterruptedException
  {
    SlashCommand command = contextMenuCommands.get(messageContextMenuInteraction.getCommandName());
    if (command != null) {
      log.info("Slash command " + command.getCommandName() + " detected by " + messageContextMenuInteraction.getUser().getName());
      command.executeContextMenu(channelId, messageContextMenuInteraction);
    } else {
      log.info("Command unrecognized, ignoring");
    }
  }

  public void cleanUp()
  {
    commands.values().forEach(SlashCommand::cleanUp);
  }

  public void registerCommandsOnDiscord()
  {
    log.info("Registering slash commands");
    commands.values().forEach(cmd -> discordService.registerSlashCommand(cmd.getCommandName(), cmd.getDescription(), cmd.getOptions()));
    commands.values().forEach(cmd ->
    {
      for (String contextMenuCommandName : cmd.getMessageContextMenuCommands()) {
        discordService.registerMessageContextMenuCommand(contextMenuCommandName);
      }
    });
    log.info("All slash commands registered");
  }
}
