package com.ethernity.kamabot.slashcommand;

import lombok.SneakyThrows;
import org.javacord.api.event.interaction.MessageContextMenuCommandEvent;
import org.javacord.api.event.interaction.ModalSubmitEvent;
import org.javacord.api.event.interaction.SlashCommandCreateEvent;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.listener.interaction.MessageContextMenuCommandListener;
import org.javacord.api.listener.interaction.ModalSubmitListener;
import org.javacord.api.listener.interaction.SlashCommandCreateListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SlashCommandsDispatcher implements SlashCommandCreateListener, ModalSubmitListener, MessageContextMenuCommandListener
{

  @Autowired
  private SlashCommandRegistry slashCommandRegistry;

  @SneakyThrows
  @Override
  public void onSlashCommandCreate(SlashCommandCreateEvent event)
  {
    SlashCommandInteraction slashCommandInteraction = event.getSlashCommandInteraction();

    slashCommandRegistry.execute(slashCommandInteraction.getChannel().get().getIdAsString(), slashCommandInteraction);
  }

  @SneakyThrows
  @Override
  public void onModalSubmit(ModalSubmitEvent event)
  {
    slashCommandRegistry.executeModal(event.getModalInteraction().getChannel().get().getIdAsString(), event.getModalInteraction());
  }

  @SneakyThrows
  @Override
  public void onMessageContextMenuCommand(MessageContextMenuCommandEvent event)
  {
    slashCommandRegistry.executeContextMenu(event.getMessageContextMenuInteraction().getChannel().get().getIdAsString(), event.getMessageContextMenuInteraction());
  }
}
