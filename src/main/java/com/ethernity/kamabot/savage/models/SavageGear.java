package com.ethernity.kamabot.savage.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.annotations.Nullable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.*;

@Data
@NoArgsConstructor
@Slf4j
public class SavageGear
{
  @Nullable
  private Boolean weapon;
  @Nullable
  private Boolean head;
  @Nullable
  private Boolean body;
  @Nullable
  private Boolean hands;
  @Nullable
  private Boolean legs;
  @Nullable
  private Boolean foots;
  @Nullable
  private Boolean necklace;
  @Nullable
  private Boolean earpiece;
  @Nullable
  private Boolean wristband;
  @Nullable
  private Boolean left_ring;
  @Nullable
  private Boolean right_ring;


  @Exclude
  @Nullable
  public Boolean isItemLooted(String itemCode)
  {
    switch (itemCode) {
      case WEAPON:
        return weapon;
      case HEAD:
        return head;
      case BODY:
        return body;
      case HANDS:
        return hands;
      case LEGS:
        return legs;
      case FOOTS:
        return foots;
      case NECKLACE:
        return necklace;
      case EARPIECE:
        return earpiece;
      case WRISTBAND:
        return wristband;
      case LEFT_RING:
        return left_ring;
      case RIGHT_RING:
        return right_ring;
      default:
        log.error("Trying to get value for non existing item " + itemCode);
        return null;
    }
  }

  @Exclude
  public void setItemLooted(String itemCode, Boolean value)
  {
    switch (itemCode) {
      case WEAPON:
        weapon = value;
        break;
      case HEAD:
        head = value;
        break;
      case BODY:
        body = value;
        break;
      case HANDS:
        hands = value;
        break;
      case LEGS:
        legs = value;
        break;
      case FOOTS:
        foots = value;
        break;
      case NECKLACE:
        necklace = value;
        break;
      case EARPIECE:
        earpiece = value;
        break;
      case WRISTBAND:
        wristband = value;
        break;
      case LEFT_RING:
        left_ring = value;
        break;
      case RIGHT_RING:
        right_ring = value;
        break;
      default:
        log.error("Trying to set loot value for non existing item " + itemCode);
        break;
    }
  }
}
