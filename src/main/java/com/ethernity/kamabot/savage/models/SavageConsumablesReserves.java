package com.ethernity.kamabot.savage.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode
public class SavageConsumablesReserves
{
  private Map<String, Long> items = new HashMap<>();

  private Long updateTimestamp;
}
