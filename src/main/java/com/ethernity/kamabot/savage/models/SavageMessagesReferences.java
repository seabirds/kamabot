package com.ethernity.kamabot.savage.models;

import com.ethernity.kamabot.dto.MessageReference;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class SavageMessagesReferences
{
  private MessageReference perItemMessageRef;
  private MessageReference perFloorMessageRef;
  private MessageReference perPlayerMessageRef;
  private MessageReference consumablesMessageRef;
  private MessageReference historyMessageRef;
}
