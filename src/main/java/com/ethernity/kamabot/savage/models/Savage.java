package com.ethernity.kamabot.savage.models;

import com.ethernity.kamabot.dto.FirebaseData;
import com.ethernity.kamabot.utils.DateUtils;
import com.google.firebase.database.Exclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.AGENT;
import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.FIBER;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Savage implements FirebaseData
{
  private String id;

  private SavageMessagesReferences messagesReferences;

  private Long startingTimestamp;

  // Map<Player, Map<Item, IsLooted>>
  private Map<String, SavagePlayer> players;

  private SavageConsumablesReserves consumablesReserves;

  private List<SavageHistory> history;

  public SavagePlayer getPlayer(String playerId)
  {
    if (players == null) {
      players = new HashMap<>();
    }
    if (!players.containsKey(playerId)) {
      players.put(playerId, new SavagePlayer());
    }
    return players.get(playerId);
  }

  public SavageConsumablesReserves getConsumablesReserves()
  {
    if (consumablesReserves == null) {
      consumablesReserves = new SavageConsumablesReserves();
    }
    return consumablesReserves;
  }

  public void declareNeed(String playerId, String item)
  {
    declareLoot(playerId, item, false);
  }

  public void declareLoot(String playerId, String item)
  {
    declareLoot(playerId, item, true);
  }

  public void declareLoot(String playerId, String item, boolean isLooted)
  {
    SavagePlayer playerNeeds = getPlayer(playerId);
    if (item.equals(FIBER) || item.equals(AGENT)) {
      playerNeeds.addUpgradeTokens(item);
    } else {
      playerNeeds.setItemLooted(item, isLooted);
    }
  }

  @Exclude
  public Map<String, List<SavageHistory>> getHistoryByFloor()
  {
    Map<String, List<SavageHistory>> result = new HashMap<>();
    history.forEach(h ->
    {
      if (!result.containsKey(h.getFloor())) {
        result.put(h.getFloor(), new ArrayList<>());
      }
      result.get(h.getFloor()).add(h);
    });
    return result;
  }

  public boolean hasPlayerLootedItem(String player, String itemCode)
  {
    return players.containsKey(player) && players.get(player).isItemLooted(itemCode);
  }

  public void declareConsumable(String playerId, String item, Long quantity)
  {
    getPlayer(playerId).declareConsumable(item, quantity);
  }

  public void declareReserve(String item, Long quantity)
  {
    getConsumablesReserves().getItems().put(item, quantity);
    getConsumablesReserves().setUpdateTimestamp(DateUtils.nowAsTimestamp());
  }

  public void declareBook(String playerId, String floor, Long quantity)
  {
    getPlayer(playerId).declareBook(floor, quantity);
  }

  public LocalDateTime startingDate()
  {
    return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.startingTimestamp), TimeZone.getDefault().toZoneId());
  }

  public LocalDate startingLocalDate()
  {
    return Instant.ofEpochMilli(this.startingTimestamp).atZone(ZoneId.systemDefault()).toLocalDate();
  }
}
