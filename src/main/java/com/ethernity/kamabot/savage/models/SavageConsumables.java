package com.ethernity.kamabot.savage.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SavageConsumables
{
  private Long potions;
  private Long food;
  private Long foodId;
  private Long potionId;
  private Long updateTimestamp;


}
