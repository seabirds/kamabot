package com.ethernity.kamabot.savage.models;

import com.google.firebase.database.Exclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.*;

@Data
@NoArgsConstructor
@Slf4j
public class SavageUpgradeTokens
{
  private Integer weaponPotion;
  private Integer weaponPotionObtained;
  private Integer fiber;
  private Integer fiberObtained;
  private Integer agent;
  private Integer agentObtained;

  @Exclude
  public Integer getRemainingWeaponPotion()
  {
    return getWeaponPotion() - getWeaponPotionObtained();
  }

  @Exclude
  public Integer getRemainingFiber()
  {
    return getFiber() - getFiberObtained();
  }

  @Exclude
  public Integer getRemainingAgent()
  {
    return getAgent() - getAgentObtained();
  }

  @Exclude
  public void addUpgradeTokens(String enhancementCode)
  {
    switch (enhancementCode) {
      case FIBER:
        if (fiberObtained == null) {
          fiberObtained = 0;
        }
        fiberObtained++;
        break;

      case AGENT:
        if (agentObtained == null) {
          agentObtained = 0;
        }
        agentObtained++;
        break;

      case WEAPON_POTION:
        if (weaponPotionObtained == null) {
          weaponPotionObtained = 0;
        }
        weaponPotionObtained++;
        break;

      default:
        log.error("Trying to declare unknown enhancement looted " + enhancementCode);
    }
  }
}
