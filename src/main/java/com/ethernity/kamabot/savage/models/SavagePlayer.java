package com.ethernity.kamabot.savage.models;

import com.ethernity.kamabot.utils.DateUtils;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.annotations.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.POTIONS;
import static com.ethernity.kamabot.savage.constants.SavageMaps.ITEMS;
import static com.ethernity.kamabot.savage.constants.SavageMaps.LOOT_PER_FLOOR;

@Getter
@Setter
@NoArgsConstructor
@Slf4j
public class SavagePlayer
{
  private SavageConsumables consumables;
  private SavageGear gear;
  private SavageUpgradeTokens upgradeTokens;
  private String job;

  private Map<String, Long> books;

  @Exclude
  public Integer getRemainingWeaponPotion()
  {
    return upgradeTokens.getRemainingWeaponPotion();
  }

  @Exclude
  public Integer getRemainingFiber()
  {
    return upgradeTokens.getRemainingFiber();
  }

  @Exclude
  public Integer getRemainingAgent()
  {
    return upgradeTokens.getRemainingAgent();
  }

  @Exclude
  @Nullable
  public Boolean isItemLooted(String itemCode)
  {
    return gear.isItemLooted(itemCode);
  }

  @Exclude
  public void setItemLooted(String itemCode, Boolean value)
  {
    gear.setItemLooted(itemCode, value);
  }

  @Exclude
  public void addUpgradeTokens(String upgradeToken)
  {
    upgradeTokens.addUpgradeTokens(upgradeToken);
  }

  @Exclude
  public Map<String, Boolean> getItemNeedsPerFloor(String floorCode)
  {
    Map<String, Boolean> itemNeeds = new HashMap<>();
    for (String item : LOOT_PER_FLOOR.get(floorCode)) {
      if (isItemLooted(item) != null) {
        itemNeeds.put(item, isItemLooted(item));
      }
    }
    return itemNeeds;
  }

  @Exclude
  public Map<String, Boolean> getItemNeeds()
  {
    Map<String, Boolean> itemNeeds = new HashMap<>();
    for (String item : ITEMS) {
      if (isItemLooted(item) != null) {
        itemNeeds.put(item, isItemLooted(item));
      }
    }
    return itemNeeds;
  }

  public void declareBook(String floor, Long quantity)
  {
    if (books == null) {
      books = new HashMap<>();
    }

    books.put(floor, quantity);
  }

  public void declareConsumable(String item, Long quantity)
  {
    if (consumables == null) {
      consumables = new SavageConsumables();
    }

    if (Objects.equals(item, POTIONS)) {
      consumables.setPotions(quantity);
    } else {
      consumables.setFood(quantity);
    }
    consumables.setUpdateTimestamp(DateUtils.nowAsTimestamp());
  }
}
