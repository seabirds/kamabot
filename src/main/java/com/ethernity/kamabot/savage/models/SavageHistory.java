package com.ethernity.kamabot.savage.models;

import com.ethernity.kamabot.utils.DateUtils;
import com.google.firebase.database.Exclude;
import lombok.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class SavageHistory
{
  public SavageHistory(String playerId, String item, String floor)
  {
    this.timestamp = Timestamp.valueOf(LocalDate.now().atStartOfDay()).getTime();
    this.playerId = playerId;
    this.item = item;
    this.floor = floor;
  }

  private long timestamp;
  private String item;
  private String playerId;
  private String floor;

  @Exclude
  public LocalDateTime getDate()
  {
    return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.timestamp), TimeZone.getDefault().toZoneId());
  }

  @Exclude
  public LocalDate getLocalDate()
  {
    return Instant.ofEpochMilli(this.timestamp).atZone(ZoneId.systemDefault()).toLocalDate();
  }

  @Exclude
  public String getDateAsString()
  {
    return DateUtils.dateFormatter.format(getDate());
  }

  @Exclude
  public int getWeek(LocalDate startingDate)
  {
    return DateUtils.getWeekNumberByStartingDate(getLocalDate(), startingDate);
  }
}
