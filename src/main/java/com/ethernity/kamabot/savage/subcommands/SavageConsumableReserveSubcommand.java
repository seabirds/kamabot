package com.ethernity.kamabot.savage.subcommands;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.SavageRepository;
import com.ethernity.kamabot.savage.models.Savage;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.ethernity.kamabot.utils.TextUtils.translate;

@Component
public class SavageConsumableReserveSubcommand
{
  @Autowired
  private SavageRepository savageRepository;
  @Autowired
  private DiscordService discordService;

  public void execute(SlashCommandInteraction slashCommandInteraction, Savage savage, String item, Long quantity)
  {
    savage.declareReserve(item, quantity);
    this.savageRepository.save(savage);
    discordService.respondEphemeralMessage(slashCommandInteraction, "Consommables " + translate(item) + "  mis à jour");
  }
}
