package com.ethernity.kamabot.savage.subcommands;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.dto.MessageReference;
import com.ethernity.kamabot.savage.SavageRepository;
import com.ethernity.kamabot.savage.mappers.SavageConsumableEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavageHistoryEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavagePerFloorEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavagePerPlayerEmbedMapper;
import com.ethernity.kamabot.savage.models.Savage;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SavageMapSubcommand
{
  @Autowired
  private SavageRepository savageRepository;

  @Autowired
  private DiscordService discordService;

  @Autowired
  private SavageConsumableEmbedMapper savageConsumableEmbedMapper;
  @Autowired
  private SavagePerFloorEmbedMapper savagePerFloorEmbedMapper;
  @Autowired
  private SavagePerPlayerEmbedMapper savagePerPlayerEmbedMapper;
  @Autowired
  private SavageHistoryEmbedMapper savageHistoryEmbedMapper;

  public void execute(SlashCommandInteraction slashCommandInteraction, Savage savage)
  {
    if (savage.getMessagesReferences().getPerItemMessageRef() != null) {
      discordService.deleteMessage(savage.getMessagesReferences().getPerItemMessageRef());
    }

    // Check if those calls are to be used after all save will retrigger it
//    Message perItemMessage = discordService.sendEmbed(slashCommandInteraction.getChannel().get(), toPerItemEmbed(savage));
    TextChannel textChannel = slashCommandInteraction.getChannel().get();
    Message perFloorMessage = discordService.sendEmbed(textChannel, savagePerFloorEmbedMapper.toPerFloorEmbed(savage));
    Message perPlayerMessage = discordService.sendEmbed(textChannel, savagePerPlayerEmbedMapper.toPerPlayerEmbed(savage));
    Message consumablesMessage = discordService.sendEmbed(textChannel, savageConsumableEmbedMapper.toConsumablesEmbed(savage));
    Message historyMessage = discordService.sendEmbed(textChannel, savageHistoryEmbedMapper.toHistoryEmbed(savage));


//    savage.setPerItemMessageRef(new MessageReference(perItemMessage.getIdAsString(), perItemMessage.getChannel().getIdAsString()));
    savage.getMessagesReferences().setPerFloorMessageRef(new MessageReference(perFloorMessage.getIdAsString(), perFloorMessage.getChannel().getIdAsString()));
    savage.getMessagesReferences().setPerPlayerMessageRef(new MessageReference(perPlayerMessage.getIdAsString(), perPlayerMessage.getChannel().getIdAsString()));
    savage.getMessagesReferences().setConsumablesMessageRef(new MessageReference(consumablesMessage.getIdAsString(), consumablesMessage.getChannel().getIdAsString()));
    savage.getMessagesReferences().setHistoryMessageRef(new MessageReference(historyMessage.getIdAsString(), historyMessage.getChannel().getIdAsString()));

    savageRepository.save(savage);

    discordService.respondEphemeralMessage(slashCommandInteraction, "Ancien message effacé et carte republiée");
  }

}
