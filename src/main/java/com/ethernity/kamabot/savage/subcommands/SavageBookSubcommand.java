package com.ethernity.kamabot.savage.subcommands;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.SavageRepository;
import com.ethernity.kamabot.savage.models.Savage;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SavageBookSubcommand
{
  @Autowired
  private SavageRepository savageRepository;
  @Autowired
  private DiscordService discordService;

  public void execute(SlashCommandInteraction slashCommandInteraction, Savage savage, String playerId, String floor, Long quantity)
  {
    savage.declareBook(playerId, floor, quantity);
    this.savageRepository.save(savage);
    discordService.respondEphemeralMessage(slashCommandInteraction, "Livres pour " + floor + "  mis à jour");
  }
}
