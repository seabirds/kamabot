package com.ethernity.kamabot.savage.subcommands;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.SavageRepository;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.models.SavageHistory;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import static com.ethernity.kamabot.savage.constants.SavageMaps.FLOOR_PER_LOOT;
import static com.ethernity.kamabot.utils.TextUtils.translate;

@Component
public class SavageLootSubcommand
{
  @Autowired
  private SavageRepository savageRepository;
  @Autowired
  private DiscordService discordService;

  public void execute(SlashCommandInteraction slashCommandInteraction, Savage savage, String playerId, String item)
  {
    savage.declareLoot(playerId, item);
    if (savage.getHistory() == null) {
      savage.setHistory(new ArrayList<>());
    }
    String floor = FLOOR_PER_LOOT.get(item).get(0);
    savage.getHistory().add(new SavageHistory(playerId, item, floor));

    this.savageRepository.save(savage);
    discordService.respondEphemeralMessage(slashCommandInteraction, "Lot de " + translate(item) + " enregistré");
  }
}
