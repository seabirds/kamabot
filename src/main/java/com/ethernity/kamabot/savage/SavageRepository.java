package com.ethernity.kamabot.savage;

import com.ethernity.kamabot.dto.FirebaseRepository;
import com.ethernity.kamabot.savage.models.Savage;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Component;

@Component
public class SavageRepository extends FirebaseRepository<Savage>
{
  public SavageRepository(FirebaseDatabase firebaseDatabase)
  {
    super(firebaseDatabase, "savage");
  }
}
