package com.ethernity.kamabot.savage;

import com.ethernity.kamabot.savage.mappers.SavageConsumableEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavageHistoryEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavagePerFloorEmbedMapper;
import com.ethernity.kamabot.savage.mappers.SavagePerPlayerEmbedMapper;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.subcommands.*;
import com.ethernity.kamabot.slashcommand.SlashCommand;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.entity.message.Message;
import org.javacord.api.interaction.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.ethernity.kamabot.savage.constants.SavageFloorConstants.*;
import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.*;
import static com.ethernity.kamabot.utils.TextUtils.translate;

@Component
@Slf4j
public class SavageSlashCommand extends SlashCommand
{
  private final static String SUBCOMMAND_LOOT = "loot";
  private final static String SUBCOMMAND_MAP = "map";
  private final static String SUBCOMMAND_NEED = "besoin";

  private final static String SUBCOMMAND_CONSUMABLE = "consommables";
  private final static String SUBCOMMAND_RESERVES = "reserves";
  private final static String SUBCOMMAND_BOOK = "tomes";

  @Autowired
  private SavageRepository savageRepository;

  @Autowired
  private SavageMapSubcommand savageMapSubcommand;
  @Autowired
  private SavageNeedSubcommand savageNeedSubcommand;
  @Autowired
  private SavageLootSubcommand savageLootSubcommand;
  @Autowired
  private SavageConsumableSubcommand savageConsumableSubcommand;
  @Autowired
  private SavageConsumableReserveSubcommand savageConsumableReserveSubcommand;
  @Autowired
  private SavageBookSubcommand savageBookSubcommand;

  @Autowired
  private SavageConsumableEmbedMapper savageConsumableEmbedMapper;
  @Autowired
  private SavagePerPlayerEmbedMapper savagePerPlayerEmbedMapper;
  @Autowired
  private SavagePerFloorEmbedMapper savagePerFloorEmbedMapper;
  @Autowired
  private SavageHistoryEmbedMapper savageHistoryEmbedMapper;

  private Savage currentSavageInProgress;

  @Override
  public void setup()
  {
    savageRepository.getValueAsStream("64", savage ->
    {
      log.info("Detected update of savage value, triggering messages update");
      this.currentSavageInProgress = savage;

      // Updating the messages with the latest data
//      if (savage.getPerItemMessageRef() != null) {
//        Message perItemMessage = discordService.getMessage(savage.getPerItemMessageRef());
//        if (perItemMessage != null) {
//          perItemMessage.edit(savageMapSubcommand.toPerItemEmbed(savage));
//        }
//      }
      if (savage.getMessagesReferences().getPerPlayerMessageRef() != null) {
        Message perPlayerMessage = discordService.getMessage(savage.getMessagesReferences().getPerPlayerMessageRef());
        if (perPlayerMessage != null) {
          perPlayerMessage.edit(savagePerPlayerEmbedMapper.toPerPlayerEmbed(savage));
        }
      }
      if (savage.getMessagesReferences().getPerFloorMessageRef() != null) {
        Message perFloorMessage = discordService.getMessage(savage.getMessagesReferences().getPerFloorMessageRef());
        if (perFloorMessage != null) {
          perFloorMessage.edit(savagePerFloorEmbedMapper.toPerFloorEmbed(savage));
        }
      }
      if (savage.getMessagesReferences().getConsumablesMessageRef() != null) {
        Message consumableMessage = discordService.getMessage(savage.getMessagesReferences().getConsumablesMessageRef());
        if (consumableMessage != null) {
          consumableMessage.edit(savageConsumableEmbedMapper.toConsumablesEmbed(savage));
        }
      }
      if (savage.getMessagesReferences().getHistoryMessageRef() != null) {
        Message historyMessage = discordService.getMessage(savage.getMessagesReferences().getHistoryMessageRef());
        if (historyMessage != null) {
          historyMessage.edit(savageHistoryEmbedMapper.toHistoryEmbed(savage));
        }
      }
    });
  }

  @Override
  public void execute(String channelId, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException
  {
    slashCommandInteraction.getOptions();
    log.info("Logged command Savage");

    List<SlashCommandInteractionOption> subcommandOptions = slashCommandInteraction.getOptions();
    slashCommandInteraction.getOptions().forEach(option -> log.info("Logged subcommand " + option.getName()));

    if (subcommandOptions.size() != 1) {
      respondGenericError(slashCommandInteraction);
      return;
    }

    SlashCommandInteractionOption subcommand = subcommandOptions.get(0);
    String playerIdAsString = slashCommandInteraction.getUser().getIdAsString();

    switch (subcommand.getName()) {
      case SUBCOMMAND_MAP:
        savageMapSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress);
        break;

      case SUBCOMMAND_NEED: {
        String item = subcommand.getOptionByName("item").get().getStringValue().get();
        savageNeedSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress, playerIdAsString, item);
      }
      break;

      case SUBCOMMAND_CONSUMABLE: {
        String item = subcommand.getOptionByName("item").get().getStringValue().get();
        Long quantity = subcommand.getOptionByName("quantity").get().getLongValue().get();
        savageConsumableSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress, playerIdAsString, item, quantity);
      }
      break;

      case SUBCOMMAND_RESERVES: {
        String item = subcommand.getOptionByName("item").get().getStringValue().get();
        Long quantity = subcommand.getOptionByName("quantity").get().getLongValue().get();
        savageConsumableReserveSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress, item, quantity);
      }
      break;

      case SUBCOMMAND_BOOK: {
        String floor = subcommand.getOptionByName("floor").get().getStringValue().get();
        Long quantity = subcommand.getOptionByName("quantity").get().getLongValue().get();
        savageBookSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress, playerIdAsString, floor, quantity);
      }
      break;

      case SUBCOMMAND_LOOT: {
        String item = subcommand.getOptionByName("item").get().getStringValue().get();
        savageLootSubcommand.execute(slashCommandInteraction, this.currentSavageInProgress, playerIdAsString, item);
      }
      break;
    }

  }

  @Override
  public String getCommandName()
  {
    return "savage";
  }

  @Override
  public String getDescription()
  {
    return "Gère le loot du savage de FFXIV";
  }

  @Override
  public List<SlashCommandOption> getOptions()
  {
    List<SlashCommandOptionChoice> itemsChoices = Arrays.asList(
        SlashCommandOptionChoice.create(translate(WEAPON), WEAPON),
        SlashCommandOptionChoice.create(translate(HEAD), HEAD),
        SlashCommandOptionChoice.create(translate(BODY), BODY),
        SlashCommandOptionChoice.create(translate(HANDS), HANDS),
        SlashCommandOptionChoice.create(translate(LEGS), LEGS),
        SlashCommandOptionChoice.create(translate(FOOTS), FOOTS),
        SlashCommandOptionChoice.create(translate(NECKLACE), NECKLACE),
        SlashCommandOptionChoice.create(translate(EARPIECE), EARPIECE),
        SlashCommandOptionChoice.create(translate(WRISTBAND), WRISTBAND),
        SlashCommandOptionChoice.create(translate(LEFT_RING), LEFT_RING),
        SlashCommandOptionChoice.create(translate(FIBER), FIBER),
        SlashCommandOptionChoice.create(translate(AGENT), AGENT)
    );
    List<SlashCommandOptionChoice> consumablesChoices = Arrays.asList(
        SlashCommandOptionChoice.create(translate(POTIONS), POTIONS),
        SlashCommandOptionChoice.create(translate(FOOD), FOOD)
    );
    List<SlashCommandOptionChoice> reservesChoices = Arrays.asList(
        SlashCommandOptionChoice.create(translate(STRENGTH_POTION), STRENGTH_POTION),
        SlashCommandOptionChoice.create(translate(INTELLIGENCE_POTION), INTELLIGENCE_POTION),
        SlashCommandOptionChoice.create(translate(DEXTERITY_POTION), DEXTERITY_POTION),
        SlashCommandOptionChoice.create(translate(SPIRIT_POTION), SPIRIT_POTION),
        SlashCommandOptionChoice.create(translate(CARROT_PUDDING), CARROT_PUDDING)
    );

    List<SlashCommandOptionChoice> floorChoices = Arrays.asList(
        SlashCommandOptionChoice.create(P9S, P9S),
        SlashCommandOptionChoice.create(P10S, P10S),
        SlashCommandOptionChoice.create(P11S, P11S),
        SlashCommandOptionChoice.create(P12S, P12S)
    );

    return Arrays.asList(
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, "associate", "Associe son compte à un personnage"),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_MAP, "Poste un nouveau message de résumé auto-mis à jour"),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_NEED, "Déclare un besoin d'objet",
            Collections.singletonList(
                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "item", "Objet", true, itemsChoices)
            )
        ),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_LOOT, "Déclare le loot d'un objet donné",
            Collections.singletonList(
                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "item", "Objet", true, itemsChoices)
            )
        ),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_CONSUMABLE, "Déclare la quantité de consommables restant",
            Arrays.asList(
                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "item", "Objet", true, consumablesChoices),
                SlashCommandOption.createWithChoices(SlashCommandOptionType.LONG, "quantity", "Quantité", true)
            )
        ),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_RESERVES, "Déclare la quantité de consommables dans le coffre en réserve",
            Arrays.asList(
                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "item", "Objet", true, reservesChoices),
                SlashCommandOption.createWithChoices(SlashCommandOptionType.LONG, "quantity", "Quantité", true)
            )
        ),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_BOOK, "Déclare la quantité de livres de savage possédés",
            Arrays.asList(
                SlashCommandOption.createWithChoices(SlashCommandOptionType.STRING, "floor", "Etage", true, floorChoices),
                SlashCommandOption.createWithChoices(SlashCommandOptionType.LONG, "quantity", "Quantité", true)
            )
        )
    );
  }
}
