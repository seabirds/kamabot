package com.ethernity.kamabot.savage.mappers;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.models.SavageConsumables;
import com.ethernity.kamabot.savage.models.SavageConsumablesReserves;
import com.ethernity.kamabot.utils.DateUtils;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static com.ethernity.kamabot.savage.constants.SavageMaps.KEY_ORDER_COMPARATOR;
import static com.ethernity.kamabot.utils.TextUtils.translate;

@Component
public class SavageConsumableEmbedMapper
{
  @Autowired
  private DiscordService discordService;

  public EmbedBuilder toConsumablesEmbed(Savage savage)
  {
    EmbedBuilder embed = new EmbedBuilder();
    embed.setTitle("Consommables");

    if (savage.getPlayers() != null) {
      savage.getPlayers().forEach((playerId, playerData) ->
      {
        String playerDisplayName = discordService.getUserNickname(playerId);
        LocalDate updateDate = playerData.getConsumables() != null && playerData.getConsumables().getUpdateTimestamp() != null ? DateUtils.timestampAsLocalDate(playerData.getConsumables().getUpdateTimestamp()) : null;
        buildConsumableField(embed, playerData.getConsumables(), updateDate, playerDisplayName);
      });
    }

    if (savage.getConsumablesReserves() != null) {
      SavageConsumablesReserves consumablesReserves = savage.getConsumablesReserves();
      Long updateTimestamp = consumablesReserves.getUpdateTimestamp() != null ? consumablesReserves.getUpdateTimestamp() : null;
      LocalDate updateDate = updateTimestamp != null ? DateUtils.timestampAsLocalDate(updateTimestamp) : null;
      buildReserveField(embed, consumablesReserves, updateDate);
    }

    return embed;
  }

  private void buildConsumableField(EmbedBuilder embed, SavageConsumables consumableTable, LocalDate updateDate, String player)
  {
    String consumableConcat;
    String updatedSinceAsString = DateUtils.buildUpdatedSinceMessage(updateDate);

    if (consumableTable == null) {
      consumableConcat = "Pas de consommables enregistré";
    } else {
      consumableConcat = new StringBuilder()
          .append("Potions : ").append(consumableTable.getPotions())
          .append("\nNourriture : ").append(consumableTable.getFood())
          .toString();
    }
    embed.addInlineField(player + " - " + updatedSinceAsString, consumableConcat);
  }

  private void buildReserveField(EmbedBuilder embed, SavageConsumablesReserves reservesTable, LocalDate updateDate)
  {
    String consumableConcat;
    String updatedSinceAsString = DateUtils.buildUpdatedSinceMessage(updateDate);

    if (reservesTable == null) {
      consumableConcat = "Pas de réserves de consommable";
    } else {
      consumableConcat = reservesTable.getItems().keySet()
          .stream()
          .sorted(KEY_ORDER_COMPARATOR)
          .map(consumable -> translate(consumable) + " : " + reservesTable.getItems().get(consumable))
          .collect(Collectors.joining("\n"));
    }
    embed.addInlineField("Réserves - " + updatedSinceAsString, consumableConcat);
  }
}
