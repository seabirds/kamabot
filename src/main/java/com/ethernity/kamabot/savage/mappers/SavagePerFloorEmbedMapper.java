package com.ethernity.kamabot.savage.mappers;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.models.SavagePlayer;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

import static com.ethernity.kamabot.savage.constants.SavageFloorConstants.*;
import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.AGENT;
import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.FIBER;
import static com.ethernity.kamabot.utils.TextUtils.*;

@Component
public class SavagePerFloorEmbedMapper
{
  @Autowired
  private DiscordService discordService;

  public EmbedBuilder toPerFloorEmbed(Savage savage)
  {
    EmbedBuilder embed = new EmbedBuilder();
    embed.setTitle("Besoins du Pandaemonium, par étage");

    buildNewPerFloorEmbedField(embed, savage, P9S);
    buildNewPerFloorEmbedField(embed, savage, P10S);
    buildNewPerFloorEmbedField(embed, savage, P11S);
    buildNewPerFloorEmbedField(embed, savage, P12S);

    return embed;
  }

  private long computeItemRemainingForPlayer(SavagePlayer savageNeed, String floorCode)
  {
    Map<String, Boolean> itemNeedsPerFloor = savageNeed.getItemNeedsPerFloor(floorCode);

    long itemsRemaining = itemNeedsPerFloor.entrySet().stream().filter(entry -> !entry.getValue()).count();

    if (floorCode.equals(P10S)) {
      itemsRemaining += savageNeed.getRemainingAgent();
    }
    if (floorCode.equals(P11S)) {
      itemsRemaining += savageNeed.getRemainingFiber();
      ;
    }

    return itemsRemaining;
  }

  private void buildNewPerFloorEmbedField(EmbedBuilder embed, Savage savage, String floorCode)
  {
    String playerLines = savage.getPlayers().keySet()
        .stream()
        .sorted((p1, p2) ->
        {
          long neededForP1 = computeItemRemainingForPlayer(savage.getPlayers().get(p1), floorCode);
          long neededForP2 = computeItemRemainingForPlayer(savage.getPlayers().get(p2), floorCode);

          return (int) (neededForP2 - neededForP1);
        })
        .map(playerId -> buildPlayerLines(savage.getPlayers().get(playerId), floorCode, playerId))
        .collect(Collectors.joining("\n"));

    embed.addInlineField(floorCode, playerLines);
  }

  private String buildPlayerLines(SavagePlayer savageNeed, String floorCode, String playerId)
  {
    Map<String, Long> books = savageNeed.getBooks();
    Map<String, Boolean> itemNeedsPerFloor = savageNeed.getItemNeedsPerFloor(floorCode);

    String itemLine;
    if (!itemNeedsPerFloor.isEmpty()) {
      itemLine = itemNeedsPerFloor
          .entrySet()
          .stream()
          .map(entry ->
          {
            String item = entry.getKey();
            Boolean need = itemNeedsPerFloor.get(item);
            String itemTranslated = translate(item);
            return strikeIfTrue(itemTranslated, need == true);
          })
          .collect(Collectors.joining(", "));
    } else {
      itemLine = "Besoin de rien, envie de toi... ~";
    }

    long itemsRemaining = itemNeedsPerFloor.entrySet().stream().filter(entry -> !entry.getValue()).count();
    long totalNeeded = itemNeedsPerFloor.size();

    String enhancementLine = "";
    if (floorCode.equals(P10S)) {
      enhancementLine = buildEnhancementSubsection(savageNeed.getRemainingAgent(), savageNeed.getUpgradeTokens().getAgent(), AGENT);
      itemsRemaining += savageNeed.getRemainingAgent();
      totalNeeded += savageNeed.getUpgradeTokens().getAgent();
    }
    if (floorCode.equals(P11S)) {
      enhancementLine = buildEnhancementSubsection(savageNeed.getRemainingFiber(), savageNeed.getUpgradeTokens().getFiber(), FIBER);
      itemsRemaining += savageNeed.getRemainingFiber();
      totalNeeded += savageNeed.getUpgradeTokens().getFiber();
    }

    String separator = "--------------\n";
    String booksLine = "";
    if (books != null) {
      booksLine = "Tome " + floorCode + " : " + books.get(floorCode) + "\n";
    }

    String playerName = discordService.getUserNickname(playerId);
    return underline(playerName + " (" + itemsRemaining + "/" + totalNeeded + ")") + "\n" + itemLine + enhancementLine + "\n" + separator + booksLine;
  }

  public String buildEnhancementSubsection(Integer remaining, Integer total, String enhancementCode)
  {
    String enhancementLine = "\n " + remaining + "/" + total + " " + translate(enhancementCode);
    return strikeIfTrue(enhancementLine, remaining == 0);
  }
}
