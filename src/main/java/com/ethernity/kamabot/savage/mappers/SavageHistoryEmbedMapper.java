package com.ethernity.kamabot.savage.mappers;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.models.SavageHistory;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static com.ethernity.kamabot.savage.constants.SavageFloorConstants.*;
import static com.ethernity.kamabot.utils.TextUtils.translate;
import static com.ethernity.kamabot.utils.TextUtils.underline;

@Component
public class SavageHistoryEmbedMapper
{
  @Autowired
  private DiscordService discordService;

  public EmbedBuilder toHistoryEmbed(Savage savage)
  {
    EmbedBuilder embed = new EmbedBuilder();
    embed.setTitle("Historique de loot");

    if (savage.getHistory() != null) {
      buildPerFloorHistoryEmbedField(embed, savage, P9S);
      buildPerFloorHistoryEmbedField(embed, savage, P10S);
      buildPerFloorHistoryEmbedField(embed, savage, P11S);
      buildPerFloorHistoryEmbedField(embed, savage, P12S);
    }

    return embed;
  }

  private void buildPerFloorHistoryEmbedField(EmbedBuilder embed, Savage savage, String floorCode)
  {
    List<SavageHistory> savageHistory = savage.getHistoryByFloor().get(floorCode);
    String historyConcat;
    if (savageHistory == null) {
      historyConcat = "Pas de loot enregistré";
    } else {
      Map<String, List<SavageHistory>> byDateHistory = new HashMap<>();
      for (SavageHistory history : savageHistory) {
        String date = history.getDateAsString();
        if (!byDateHistory.containsKey(date)) {
          byDateHistory.put(date, new ArrayList<>());
        }
        byDateHistory.get(date).add(history);
      }

      historyConcat = byDateHistory.keySet()
          .stream()
          .sorted(Comparator.naturalOrder())
          .map(date -> underline(date) + "\n" + byDateHistory.get(date).stream().map(history ->
          {
            String playerName = discordService.getUserNickname(history.getPlayerId());
            return playerName + " - " + translate(history.getItem());
          }).collect(Collectors.joining("\n")))
          .collect(Collectors.joining("\n\n"));
    }
    embed.addInlineField(floorCode, historyConcat);
  }
}
