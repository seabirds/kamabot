package com.ethernity.kamabot.savage.mappers;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.savage.constants.SavageFloorOrder;
import com.ethernity.kamabot.savage.models.Savage;
import com.ethernity.kamabot.savage.models.SavagePlayer;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.*;
import static com.ethernity.kamabot.savage.constants.SavageMaps.MAP_ENTRY_ORDER_COMPARATOR;
import static com.ethernity.kamabot.utils.TextUtils.*;

@Component
public class SavagePerPlayerEmbedMapper
{
  @Autowired
  private DiscordService discordService;

  public EmbedBuilder toPerPlayerEmbed(Savage savage)
  {
    EmbedBuilder embed = new EmbedBuilder();
    embed.setTitle("Besoins du Pandaemonium, par personne");

    savage.getPlayers().forEach((playerId, needs) ->
    {
      buildPerPlayerEmbedField(embed, needs, playerId);
    });

    return embed;
  }

  private void buildPerPlayerEmbedField(EmbedBuilder embed, SavagePlayer needs, String playerId)
  {
    Map<String, Long> books = needs.getBooks();
    String itemsConcat;
    String playerDisplayName = discordService.getUserNickname(playerId);
    Map<String, Boolean> itemsNeed = needs.getItemNeeds();
    if (itemsNeed == null || itemsNeed.size() == 0) {
      embed.addInlineField(playerDisplayName, "Besoin de rien, envie de toiiiiiiiiiiiiiiiiiiiiiiiiiii");
    } else {
      long remaining = itemsNeed.entrySet().stream().filter(entry -> !entry.getValue()).count()
          + needs.getRemainingWeaponPotion() + needs.getRemainingFiber() + needs.getRemainingAgent();
      long total = itemsNeed.size() + needs.getUpgradeTokens().getFiber() + needs.getUpgradeTokens().getAgent();

      itemsConcat = itemsNeed.entrySet()
          .stream()
          .sorted(MAP_ENTRY_ORDER_COMPARATOR)
          .map(entry ->
          {
            if (entry.getValue()) {
              return strike(translate(entry.getKey()));
            } else {
              return translate(entry.getKey());
            }
          }).collect(Collectors.joining("\n"));

      String weaponPotionNeed = needs.getRemainingWeaponPotion() + "/" + needs.getUpgradeTokens().getWeaponPotion() + " " + translate(WEAPON_POTION);
      String fiberNeeds = needs.getRemainingFiber() + "/" + needs.getUpgradeTokens().getFiber() + " " + translate(FIBER);
      String agentsNeeds = needs.getRemainingAgent() + "/" + needs.getUpgradeTokens().getAgent() + " " + translate(AGENT);

      itemsConcat += "\n" + strikeIfTrue(weaponPotionNeed, needs.getRemainingWeaponPotion() == 0);
      itemsConcat += "\n" + strikeIfTrue(fiberNeeds, needs.getRemainingFiber() == 0);
      itemsConcat += "\n" + strikeIfTrue(agentsNeeds, needs.getRemainingAgent() == 0);

      String separator = "\n--------------";
      String booksLines = "";
      if (books != null) {
        booksLines = "\n" + books.keySet().stream().sorted(SavageFloorOrder.COMPARATOR).map(floor -> "Tome " + floor + " : " + books.get(floor)).collect(Collectors.joining("\n")) + "\n";
      }

      String playerName = discordService.getUserNickname(playerId);
      embed.addInlineField(playerName + " (" + remaining + "/" + total + ")", itemsConcat + separator + booksLines);
    }
  }
}
