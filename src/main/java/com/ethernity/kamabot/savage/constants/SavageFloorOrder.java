package com.ethernity.kamabot.savage.constants;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static com.ethernity.kamabot.savage.constants.SavageFloorConstants.*;

public class SavageFloorOrder
{
  public final static Map<String, Integer> FLOOR_ORDER = new HashMap<>();

  static {
    FLOOR_ORDER.put(P9S, 100);
    FLOOR_ORDER.put(P10S, 200);
    FLOOR_ORDER.put(P11S, 300);
    FLOOR_ORDER.put(P12S, 400);
  }

  public final static Comparator<String> COMPARATOR = Comparator.comparingInt(FLOOR_ORDER::get);
}
