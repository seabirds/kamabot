package com.ethernity.kamabot.savage.constants;

import java.util.*;

import static com.ethernity.kamabot.savage.constants.SavageFloorConstants.*;
import static com.ethernity.kamabot.savage.constants.SavageItemsConstants.*;

public class SavageMaps
{
  public final static String ETHERNITY_NAME = "ETHERNITY";

  public final static List<String> ITEMS = new ArrayList<>();

  static {
    ITEMS.add(WEAPON);
    ITEMS.add(HEAD);
    ITEMS.add(BODY);
    ITEMS.add(HANDS);
    ITEMS.add(LEGS);
    ITEMS.add(FOOTS);
    ITEMS.add(EARPIECE);
    ITEMS.add(NECKLACE);
    ITEMS.add(WRISTBAND);
    ITEMS.add(LEFT_RING);
    ITEMS.add(RIGHT_RING);
  }

  public final static Map<String, String> ITEMS_TRANSLATIONS = new HashMap<>();

  static {
    ITEMS_TRANSLATIONS.put(WEAPON, "Arme");
    ITEMS_TRANSLATIONS.put(HEAD, "Tête");
    ITEMS_TRANSLATIONS.put(BODY, "Corps");
    ITEMS_TRANSLATIONS.put(HANDS, "Gants");
    ITEMS_TRANSLATIONS.put(LEGS, "Jambes");
    ITEMS_TRANSLATIONS.put(FOOTS, "Pieds");
    ITEMS_TRANSLATIONS.put(EARPIECE, "Boucle d'oreille");
    ITEMS_TRANSLATIONS.put(NECKLACE, "Collier");
    ITEMS_TRANSLATIONS.put(WRISTBAND, "Bracelets");
    ITEMS_TRANSLATIONS.put(LEFT_RING, "Anneau");
    ITEMS_TRANSLATIONS.put(RIGHT_RING, "Anneau droit");
    ITEMS_TRANSLATIONS.put(WEAPON_POTION, "Potion d'arme");
    ITEMS_TRANSLATIONS.put(FIBER, "Fibre d'armure");
    ITEMS_TRANSLATIONS.put(AGENT, "Potion de bijoux");
    ITEMS_TRANSLATIONS.put(POTIONS, "Potions");
    ITEMS_TRANSLATIONS.put(FOOD, "Nourriture");
    ITEMS_TRANSLATIONS.put(CARROT_PUDDING, "Aubergine noire du mage au four");
    ITEMS_TRANSLATIONS.put(STRENGTH_POTION, "Potion de force");
    ITEMS_TRANSLATIONS.put(INTELLIGENCE_POTION, "Potion d'intelligence");
    ITEMS_TRANSLATIONS.put(DEXTERITY_POTION, "Potion de dextérité");
    ITEMS_TRANSLATIONS.put(SPIRIT_POTION, "Potion d'esprit");
  }

  public final static Map<String, Integer> ITEMS_ORDER = new HashMap<>();

  static {
    ITEMS_ORDER.put(WEAPON, 10);
    ITEMS_ORDER.put(HEAD, 20);
    ITEMS_ORDER.put(BODY, 21);
    ITEMS_ORDER.put(HANDS, 22);
    ITEMS_ORDER.put(LEGS, 23);
    ITEMS_ORDER.put(FOOTS, 24);
    ITEMS_ORDER.put(FIBER, 25);
    ITEMS_ORDER.put(EARPIECE, 30);
    ITEMS_ORDER.put(NECKLACE, 31);
    ITEMS_ORDER.put(WRISTBAND, 32);
    ITEMS_ORDER.put(LEFT_RING, 33);
    ITEMS_ORDER.put(RIGHT_RING, 34);
    ITEMS_ORDER.put(WEAPON_POTION, 35);
    ITEMS_ORDER.put(AGENT, 36);
    ITEMS_ORDER.put(CARROT_PUDDING, 100);
    ITEMS_ORDER.put(STRENGTH_POTION, 200);
    ITEMS_ORDER.put(DEXTERITY_POTION, 300);
    ITEMS_ORDER.put(INTELLIGENCE_POTION, 400);
    ITEMS_ORDER.put(SPIRIT_POTION, 500);
  }

  public final static Comparator<String> KEY_ORDER_COMPARATOR = Comparator.comparingInt(ITEMS_ORDER::get);
  public final static Comparator<Map.Entry<String, Boolean>> MAP_ENTRY_ORDER_COMPARATOR = Comparator.comparingInt(e -> ITEMS_ORDER.get(e.getKey()));

  public final static Map<String, List<String>> LOOT_PER_FLOOR = new HashMap<>();

  static {
    LOOT_PER_FLOOR.put(P9S, Arrays.asList(EARPIECE, NECKLACE, WRISTBAND, LEFT_RING, RIGHT_RING));
    LOOT_PER_FLOOR.put(P10S, Arrays.asList(HEAD, HANDS, FOOTS));
    LOOT_PER_FLOOR.put(P11S, Arrays.asList(HEAD, HANDS, FOOTS, LEGS));
    LOOT_PER_FLOOR.put(P12S, Arrays.asList(WEAPON, BODY));
  }


  public final static Map<String, List<String>> ENHANCEMENT_PER_FLOOR = new HashMap<>();

  static {
    ENHANCEMENT_PER_FLOOR.put(P9S, Collections.emptyList());
    ENHANCEMENT_PER_FLOOR.put(P10S, Collections.singletonList(AGENT));
    ENHANCEMENT_PER_FLOOR.put(P11S, Arrays.asList(FIBER, WEAPON_POTION));
    ENHANCEMENT_PER_FLOOR.put(P12S, Collections.emptyList());
  }

  public final static Map<String, List<String>> FLOOR_PER_LOOT = new HashMap<>();

  static {
    FLOOR_PER_LOOT.put(WEAPON, Collections.singletonList(P12S));
    FLOOR_PER_LOOT.put(HEAD, Collections.singletonList(P10S));
    FLOOR_PER_LOOT.put(BODY, Collections.singletonList(P11S));
    FLOOR_PER_LOOT.put(HANDS, Collections.singletonList(P10S));
    FLOOR_PER_LOOT.put(LEGS, Collections.singletonList(P11S));
    FLOOR_PER_LOOT.put(FOOTS, Collections.singletonList(P10S));
    FLOOR_PER_LOOT.put(EARPIECE, Collections.singletonList(P9S));
    FLOOR_PER_LOOT.put(NECKLACE, Collections.singletonList(P9S));
    FLOOR_PER_LOOT.put(WRISTBAND, Collections.singletonList(P9S));
    FLOOR_PER_LOOT.put(LEFT_RING, Collections.singletonList(P9S));
    FLOOR_PER_LOOT.put(RIGHT_RING, Collections.singletonList(P9S));
    FLOOR_PER_LOOT.put(WEAPON_POTION, Collections.singletonList(P11S));
    FLOOR_PER_LOOT.put(FIBER, Collections.singletonList(P10S));
    FLOOR_PER_LOOT.put(AGENT, Collections.singletonList(P11S));
  }
}
