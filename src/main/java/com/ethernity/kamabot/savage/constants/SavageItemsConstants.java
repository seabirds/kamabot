package com.ethernity.kamabot.savage.constants;

public class SavageItemsConstants
{
  public final static String WEAPON = "weapon";
  public final static String HEAD = "head";
  public final static String BODY = "body";
  public final static String HANDS = "hands";
  public final static String LEGS = "legs";
  public final static String FOOTS = "foots";
  public final static String NECKLACE = "necklace";
  public final static String EARPIECE = "earpiece";
  public final static String WRISTBAND = "wristband";
  public final static String LEFT_RING = "left_ring";
  public final static String RIGHT_RING = "right_ring";
  public final static String WEAPON_POTION = "weapon_potion";
  public final static String FIBER = "fiber";
  public final static String AGENT = "agent";

  public final static String POTIONS = "potions";

  public final static String FOOD = "food";

  public final static String CARROT_PUDDING = "carrot_pudding";
  public final static String STRENGTH_POTION = "strength_potion";
  public final static String INTELLIGENCE_POTION = "intelligence_potion";
  public final static String DEXTERITY_POTION = "dexterity_potion";
  public final static String SPIRIT_POTION = "spirit_potion";
}
