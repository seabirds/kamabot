package com.ethernity.kamabot.utils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class DateUtils
{


  public static Long nowAsTimestamp()
  {
    return Timestamp.valueOf(LocalDateTime.now()).getTime();
  }

  public static DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;

  public static LocalDateTime timestampAsLocalDateTime(Long timestamp)
  {
    return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone.getDefault().toZoneId());
  }

  public static LocalDate timestampAsLocalDate(Long timestamp)
  {
    return timestampAsLocalDateTime(timestamp).toLocalDate();
  }

  public static String buildUpdatedSinceMessage(LocalDate updateDate)
  {
    if (updateDate == null) {
      return "Jamais mis à jour";
    }
    int daysDifference = Period.between(updateDate, LocalDate.now()).getDays();
    switch (daysDifference) {
      case 0:
        return "Mis à jour aujourd'hui";

      case 1:
        return "Mis à jour hier";

      default:
        return "Mis à jour il y a " + daysDifference + "j";
    }
  }

  public static int getWeekNumberByStartingDate(LocalDate date, LocalDate startingDate)
  {
    return (Period.between(startingDate, date).getDays() / 7) + 1;
  }
}
