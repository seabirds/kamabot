package com.ethernity.kamabot.utils;

import static com.ethernity.kamabot.savage.constants.SavageMaps.ITEMS_TRANSLATIONS;

public class TextUtils
{
  public static String strike(String value)
  {
    return "~~" + value + "~~";
  }

  public static String strikeIfTrue(String message, boolean isTrue)
  {
    return isTrue ? strike(message) : message;
  }

  public static String bold(String value)
  {
    return "**" + value + "**";
  }

  public static String underline(String value)
  {
    return "__" + value + "__";
  }

  public static String translate(String value)
  {
    return ITEMS_TRANSLATIONS.getOrDefault(value, value);
  }
}
