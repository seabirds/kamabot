package com.ethernity.kamabot.patchnotes;

import com.ethernity.kamabot.command.Command;
import org.springframework.stereotype.Component;

@Component
public class PatchNotesCommand extends Command
{
  @Override
  public void execute(String channelId, String cmdText)
  {
    discordService.sendMessage(channelId, "**Kama-Bot 17/08/2022**" +
        "\n " +
        "\n- Version de développement du bot" +
        "\n- Version Slash du sondage");
  }

  @Override
  public String getCommandName()
  {
    return "PATCHNOTES";
  }
}
