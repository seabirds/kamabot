package com.ethernity.kamabot.birthday;

import com.ethernity.kamabot.dto.FirebaseRepository;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Component;

@Component
public abstract class BirthdayRepository extends FirebaseRepository<Birthday>
{

  public BirthdayRepository(FirebaseDatabase firebaseDatabase)
  {
    super(firebaseDatabase, "birthday");
  }
}
