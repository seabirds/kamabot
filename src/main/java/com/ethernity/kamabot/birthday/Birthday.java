package com.ethernity.kamabot.birthday;

import com.ethernity.kamabot.dto.FirebaseData;
import lombok.Data;

@Data
public class Birthday implements FirebaseData
{
  private String id;

  private Long messageId;

  @Override
  public String toString()
  {
    return this.getId() + " --- " + this.getMessageId();
  }
}
