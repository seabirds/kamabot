package com.ethernity.kamabot.birthday;

import com.ethernity.kamabot.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EndwalkerReminderCommand extends Command
{

  @Autowired
  private BirthdayReminderDaemon birthdayReminderDaemon;

  @Override
  public void execute(String channelId, String cmdText)
  {
    birthdayReminderDaemon.register(channelId, "Lost Ark", "08/02/2022");
  }

  @Override
  public String getCommandName()
  {
    return "LOSTARK";
  }
}
