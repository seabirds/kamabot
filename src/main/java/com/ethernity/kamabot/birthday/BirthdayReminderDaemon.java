package com.ethernity.kamabot.birthday;

import com.ethernity.kamabot.daemon.Daemon;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class BirthdayReminderDaemon extends Daemon
{

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/y");

  public void sendBirthdayMessage(Message message, String title, LocalDateTime birthday)
  {
    LocalDateTime now = LocalDateTime.now();
    Duration beforeBday = Duration.between(now, birthday);
    long millis = beforeBday.toMillis();

    String timeLeft = String.format("%02d:%02d:%02d:%02d",
        TimeUnit.MILLISECONDS.toDays(millis),
        TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

    Map<String, String> fields = new HashMap<>();
    fields.put("Temps restant", timeLeft);

    discordService.editMessage(message, title, fields);
  }

  public void register(String channelId, String title, String birthdate)
  {
    LocalDateTime parsedBirthDate = LocalDate.parse(birthdate, formatter).atTime(17, 0);

    Message message = discordService.sendMessageWithEmbed(channelId, title, new HashMap<>());

    scheduler.scheduleAtFixedRate(() -> sendBirthdayMessage(message, title, parsedBirthDate), 5000);
  }
}
