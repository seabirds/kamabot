package com.ethernity.kamabot.birthday;

import com.ethernity.kamabot.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KamalenReminderCommand extends Command
{

  @Autowired
  private BirthdayReminderDaemon birthdayReminderDaemon;

  @Override
  public void execute(String channelId, String cmdText)
  {
    birthdayReminderDaemon.register(channelId, "30 ans de Kamalen", "03/11/2022");
  }

  @Override
  public String getCommandName()
  {
    return "BDAY";
  }
}
