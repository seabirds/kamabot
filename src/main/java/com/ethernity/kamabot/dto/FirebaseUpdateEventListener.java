package com.ethernity.kamabot.dto;

public interface FirebaseUpdateEventListener<T>
{
  void onUpdate(T value);
}
