package com.ethernity.kamabot.dto;

@FunctionalInterface
public interface FirebaseListener<T>
{
  void valueUpdated(T value);
}
