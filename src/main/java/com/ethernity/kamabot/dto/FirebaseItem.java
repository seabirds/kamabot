package com.ethernity.kamabot.dto;

import lombok.Data;

@Data
public class FirebaseItem<T extends FirebaseData>
{
  private String id;
  private T data;
}
