package com.ethernity.kamabot.dto;

public interface FirebaseData
{
  public String getId();

  public void setId(String id);
}
