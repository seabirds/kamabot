package com.ethernity.kamabot.dto;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MessageReference
{
  private String messageId;
  private String channelId;
}
