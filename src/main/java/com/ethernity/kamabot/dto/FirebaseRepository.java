package com.ethernity.kamabot.dto;

import com.google.firebase.database.*;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Slf4j
public abstract class FirebaseRepository<T extends FirebaseData>
{
  private final Class<T> type;
  private final String prefix;
  private final String reference;
  protected FirebaseDatabase firebaseDatabase;

  public FirebaseRepository(FirebaseDatabase firebaseDatabase, String prefix)
  {
    this.firebaseDatabase = firebaseDatabase;
    this.prefix = prefix;
    this.reference = "/" + prefix + "/";

    ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
    Type type = genericSuperclass.getActualTypeArguments()[0];
    if (type instanceof Class) {
      this.type = (Class<T>) type;
    } else if (type instanceof ParameterizedType) {
      this.type = (Class<T>) ((ParameterizedType) type).getRawType();
    } else {
      throw new RuntimeException("");
    }
  }

  public CompletableFuture<Iterable<T>> findAll()
  {
    CompletableFuture<Iterable<T>> future = new CompletableFuture<>();
    this.firebaseDatabase.getReference(this.prefix).addValueEventListener(new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot)
      {
        ArrayList<T> result = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
          // TODO: handle the post
          result.add(postSnapshot.getValue(type));
        }

        future.complete(result);
      }

      @Override
      public void onCancelled(DatabaseError databaseError)
      {
        System.out.println("ERROR");
        // TODO
      }
    });
    return future;
  }

  public void getValueAsStream(String id, FirebaseListener<T> listener)
  {
    this.firebaseDatabase.getReference(this.reference + id).addValueEventListener(new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot snapshot)
      {
        try {
          listener.valueUpdated(snapshot.getValue(type));
        } catch (DatabaseException e) {
          log.error("Cannot reload value, model does not match", e);
        }
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        // TODO
        log.error("Error when loading Savage data :" + error.getMessage());
      }
    });
  }

  public CompletableFuture<T> getValue(String id)
  {
    CompletableFuture<T> future = new CompletableFuture<T>();
    this.firebaseDatabase.getReference(this.reference + id).addListenerForSingleValueEvent(new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot)
      {
        future.complete(dataSnapshot.getValue(type));
      }

      @Override
      public void onCancelled(DatabaseError databaseError)
      {
        // TODO
      }
    });
    return future;
  }

  /**
   * Watch over a value and trigger the event in case of changes
   * Note that watchValue will trigger an event with the first value
   */
  public void watchValue(String id, FirebaseUpdateEventListener<T> firebaseUpdateEvent)
  {
    this.firebaseDatabase.getReference("/" + this.prefix + "/" + id).addValueEventListener(new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot)
      {
        FirebaseItem<T> item = new FirebaseItem<>();
        item.setId(dataSnapshot.getKey());
        item.setData(dataSnapshot.getValue(type));
        firebaseUpdateEvent.onUpdate(dataSnapshot.getValue(type));
      }

      @Override
      public void onCancelled(DatabaseError databaseError)
      {
        // TODO
      }
    });
  }

  public Future<Void> save(T value)
  {
    DatabaseReference collection = this.firebaseDatabase.getReference(this.prefix);
    DatabaseReference element;
    if (value.getId() == null) {
      element = collection.child("").push();
    } else {
      element = collection.child(value.getId());
    }
    value.setId(element.getKey());
    return element.setValueAsync(value);
  }

  public void saveAll(Iterable<T> values)
  {

  }

  public Future<Void> delete(T value)
  {
    return this.firebaseDatabase.getReference(this.prefix).child(value.getId()).removeValueAsync();
  }
}
