package com.ethernity.kamabot.maillonfaible;

import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.slashcommand.SlashCommand;
import org.javacord.api.entity.channel.ServerVoiceChannel;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.interaction.SlashCommandOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

@Component
public class MaillonFaibleSlashCommand extends SlashCommand
{
  private final Random random = new Random();

  @Autowired
  private DiscordService discordService;

  @Override
  public void execute(String channelId, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException
  {

    ServerVoiceChannel channel = discordService.getVoiceChannel(discordIDs.generalChannel);
    List<User> connectedUsers = new ArrayList<User>(channel.getConnectedUsers());

    User toKick = connectedUsers.get(random.nextInt(connectedUsers.size()));

    channel.getServer().moveUser(toKick, null);

    slashCommandInteraction.createImmediateResponder()
        .setContent("Et le maillon faible est.... " + toKick.getDisplayName(slashCommandInteraction.getServer().get()))
        .respond();
  }

  @Override
  public String getCommandName()
  {
    return "maillonfaible";
  }

  @Override
  public String getDescription()
  {
    return "Le système va désigner et éliminer... LE MAILLON FAIBLE";
  }

  @Override
  public List<SlashCommandOption> getOptions()
  {
    return Collections.emptyList();
  }
}
