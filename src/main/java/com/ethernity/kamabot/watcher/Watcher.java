package com.ethernity.kamabot.watcher;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import org.javacord.api.entity.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Class dedicated to watch over every message and maybe react
 */
public abstract class Watcher
{
  @Autowired
  protected DiscordService discordService;
  @Autowired
  protected DiscordIDs discordIDs;
  @Autowired
  protected ThreadPoolTaskScheduler scheduler;

  public abstract String getName();

  public abstract void reactToMessage(Message message);
}