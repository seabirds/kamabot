package com.ethernity.kamabot.watcher;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;

import java.util.HashMap;
import java.util.Map;

public class WatcherRegistry
{
  private final DiscordService discordService;
  private final DiscordIDs discordIDs;

  private final Map<String, Watcher> watchers = new HashMap<>();

  public WatcherRegistry(DiscordService discordService, DiscordIDs discordIDs)
  {
    this.discordService = discordService;
    this.discordIDs = discordIDs;
  }

  public void register(Watcher watcher)
  {
    watchers.put(watcher.getName(), watcher);
  }

  public void startWatching()
  {
    discordService.getTextChannel(discordIDs.ffvixParsesChannel).addMessageCreateListener(message ->
    {
      this.watchers.values().forEach(watcher -> watcher.reactToMessage(message.getMessage()));
    });
  }
}
