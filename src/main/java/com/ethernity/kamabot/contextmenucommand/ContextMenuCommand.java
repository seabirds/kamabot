package com.ethernity.kamabot.contextmenucommand;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.interaction.SlashCommandOption;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.ExecutionException;

public abstract class ContextMenuCommand
{
  protected final String MODAL_SEPARATOR = "//";

  @Autowired
  protected DiscordService discordService;
  @Autowired
  protected DiscordIDs discordIDs;

  public abstract void execute(long channel, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException;

  public abstract String getCommandName();

  public abstract String getDescription();

  public abstract List<SlashCommandOption> getOptions();

  public void setup()
  {
    // No default operations
  }

  public void cleanUp()
  {
    // No operation maybe
  }

  public void respondGenericError(SlashCommandInteraction slashCommandInteraction)
  {
    slashCommandInteraction.createImmediateResponder()
        .setContent("La commande a été mal utilisée. Merci de corriger l'appel (ou de râler sur Kamalen)")
        .setFlags(MessageFlag.EPHEMERAL)
        .respond();
  }

  public void respondSimpleMessage(SlashCommandInteraction slashCommandInteraction, String message)
  {
    slashCommandInteraction.createImmediateResponder()
        .setContent(message)
        .respond();
  }
}
