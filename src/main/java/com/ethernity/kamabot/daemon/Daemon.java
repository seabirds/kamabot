package com.ethernity.kamabot.daemon;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Class dedicated to provide a base for background-running operations
 */
public abstract class Daemon
{
  @Autowired
  protected DiscordService discordService;
  @Autowired
  protected DiscordIDs discordIDs;
  @Autowired
  protected ThreadPoolTaskScheduler scheduler;

}
