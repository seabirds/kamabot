package com.ethernity.kamabot.wiki;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.slashcommand.SlashCommand;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.component.ActionRow;
import org.javacord.api.entity.message.component.TextInput;
import org.javacord.api.entity.message.component.TextInputStyle;
import org.javacord.api.interaction.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
@Slf4j
public class WikiSlashCommand extends SlashCommand
{
  private final static String SUBCOMMAND_EDIT = "edit";
  private final static String SUBCOMMAND_POST = "post";

  @Autowired
  private DiscordIDs discordIDs;

  @Override
  public void execute(String channelId, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException
  {
    log.info("Logged command Wiki");

    List<SlashCommandInteractionOption> subcommandOptions = slashCommandInteraction.getOptions();
    slashCommandInteraction.getOptions().forEach(option -> log.info("Logged subcommand " + option.getName()));

    if (subcommandOptions.size() != 1) {
      respondGenericError(slashCommandInteraction);
      return;
    }

    SlashCommandInteractionOption subcommand = subcommandOptions.get(0);

    switch (subcommand.getName()) {
      case SUBCOMMAND_POST: {
        Message message = discordService.sendMessage(slashCommandInteraction.getChannel().get(), "\u200b");
        message.edit("Message éditable posté. Faites clic droit > Editer le Wiki ou utilisez la commande /wiki edit pour le modifier \n\n ID : " + message.getId());
        discordService.respondEphemeralMessage(slashCommandInteraction, "Message éditable posté.");
      }
      break;

      case SUBCOMMAND_EDIT:
        String idAsStr = subcommand.getOptionByName("id").get().getStringValue().get();
        this.postEditionModal(idAsStr, slashCommandInteraction);
        break;
    }

  }

  @Override
  public void executeModal(String channelId, ModalInteraction modalInteraction)
  {
    String[] parameters = modalInteraction.getCustomId().split(MODAL_SEPARATOR);
    if (parameters.length != 3) {
      discordService.respondEphemeralMessage(modalInteraction, "Formulaire non compris");
    } else {
      String messageId = parameters[2];
      Message message = discordService.getMessage(messageId, modalInteraction.getChannel().get());
      message.edit(modalInteraction.getComponents().get(0).asActionRow().get().getComponents().get(0).asTextInput().get().getValue());
      discordService.respondEphemeralMessage(modalInteraction, "Edition réussie");
    }
  }

  @Override
  public void executeContextMenu(String channelId, MessageContextMenuInteraction messageContextMenuInteraction) throws ExecutionException, InterruptedException
  {
    Message message = messageContextMenuInteraction.getTarget();
    this.postEditionModal(message.getIdAsString(), messageContextMenuInteraction);
  }

  private void postEditionModal(String messageId, InteractionBase interaction) throws ExecutionException, InterruptedException
  {
    Message message = discordService.getMessage(messageId, interaction.getChannel().get());
    if (message == null) {
      discordService.respondEphemeralMessage(interaction, "Erreur : cet id de message n'existe pas");
    } else if (message.getAuthor().getId() != discordIDs.botId) {
      discordService.respondEphemeralMessage(interaction, "Seul les messages du bot sont éditables !");
    } else {
      interaction.respondWithModal("wiki" + MODAL_SEPARATOR + "edit" + MODAL_SEPARATOR + messageId, "Editer le message",
          ActionRow.of(TextInput.create(TextInputStyle.PARAGRAPH, "message_to_edit", "Message à modifier", "", message.getContent()))
      ).get();
    }
  }

  @Override
  public String getCommandName()
  {
    return "wiki";
  }

  @Override
  public String[] getMessageContextMenuCommands()
  {
    return new String[]{"Editer le Wiki"};
  }

  @Override
  public String getDescription()
  {
    return "Permet de poster des messages éditables par tous";
  }

  @Override
  public List<SlashCommandOption> getOptions()
  {
    return Arrays.asList(
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_EDIT, "Edite un message Wiki",
            Arrays.asList(
                SlashCommandOption.create(SlashCommandOptionType.STRING, "id", "ID du message", true)
            )
        ),
        SlashCommandOption.createWithOptions(SlashCommandOptionType.SUB_COMMAND, SUBCOMMAND_POST, "Poste un nouveau message en mode Wiki")
    );
  }
}
