package com.ethernity.kamabot.discord;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DiscordIDs
{
  @Value("${discord.server-id}")
  public String serverId;

  @Value("${discord.command-prefix}")
  public String commandPrefix;

  @Value("${discord.bot-token}")
  public String token;

  @Value("${discord.bot-channel}")
  public long botChannel;

  @Value("${discord.general-channel}")
  public long generalChannel;

  @Value("${discord.ffxiv-parses-channel}")
  public long ffvixParsesChannel;

  @Value("${discord.afk-channel}")
  public long afkChannel;

  @Value("${discord.ffxiv-raid-channel}")
  public long ffxivRaidChannel;

  @Value("${discord.ffxiv-raid-detail-message}")
  public long ffxivRaidDetailMessage;

  @Value("${discord.bot-id}")
  public long botId;
}
