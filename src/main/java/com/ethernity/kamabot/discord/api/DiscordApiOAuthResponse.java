package com.ethernity.kamabot.discord.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscordApiOAuthResponse
{
  private String access_token;
  private String token_type;
  private Long expires_in;
  private String refresh_token;
  private String scope;
}
