package com.ethernity.kamabot.discord.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class DiscordApiOAuthRefreshRequest
{
  private final String client_id;
  private final String client_secret;
  private final String refresh_token;
  private String grant_type = "refresh_token";

  public String toFormData()
  {
    return "client_id=%s&client_secret=%s&grant_type=%s&code=%s&refresh_token=%s".formatted(client_id, client_secret, grant_type, refresh_token);
  }
}
