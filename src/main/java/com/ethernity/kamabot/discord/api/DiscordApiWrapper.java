package com.ethernity.kamabot.discord.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class DiscordApiWrapper
{
  @Value("${discord.api}")
  private String discordApiUrl;
  @Value("${discord.client-id}")
  private String clientId;
  @Value("${discord.client-secret}")
  private String clientSecret;

  private WebClient http = WebClient.builder()
      .filter(logRequest())
      .build();

  private static ExchangeFilterFunction logRequest()
  {
    return ExchangeFilterFunction.ofRequestProcessor(clientRequest ->
    {
      log.info("Request: {} {} {}", clientRequest.method(), clientRequest.url(), clientRequest.body());
      clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
      return Mono.just(clientRequest);
    });
  }

  public DiscordApiOAuthResponse requireAccessToken(String accessCode)
  {
    DiscordApiOAuthAccessRequest oauthRequest = new DiscordApiOAuthAccessRequest(this.clientId, this.clientSecret, accessCode, "http://localhost:4200/login");

    return http
        .post()
        .uri(discordApiUrl + "/oauth2/token")
        .bodyValue(oauthRequest.toFormData())
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        .retrieve()
        .bodyToMono(DiscordApiOAuthResponse.class)
        .block();
  }

  public DiscordApiOAuthResponse refreshAccessToken(String refreshToken)
  {
    DiscordApiOAuthRefreshRequest oauthRequest = new DiscordApiOAuthRefreshRequest(this.clientId, this.clientSecret, refreshToken);

    return http
        .post()
        .uri(discordApiUrl + "/oauth2/token")
        .bodyValue(oauthRequest.toFormData())
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
        .retrieve()
        .bodyToMono(DiscordApiOAuthResponse.class)
        .block();
  }

  public DiscordApiUser getUserByToken(String token)
  {
    try {
      DiscordApiUser discordUser = http
          .get()
          .uri(discordApiUrl + "/oauth2/@me")
          .header("Authorization", "Bearer " + token)
          .retrieve()
          .bodyToMono(DiscordApiUser.class)
          .block();

      return discordUser;
    } catch (WebClientResponseException.Unauthorized e) {
      log.error("Unauthorized discord access token");
      return null;
    }

  }
}
