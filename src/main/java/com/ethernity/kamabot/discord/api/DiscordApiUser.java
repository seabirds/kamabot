package com.ethernity.kamabot.discord.api;

import lombok.Data;

@Data
public class DiscordApiUser
{
  private DiscordApplication application;
  private String[] scopes;
  private String expires;
  private DiscordApiUserData user;

  @Data
  public static class DiscordApiUserData
  {
    private String id;
    private String username;
    private String avatar;
    private String discriminator;
    private Integer public_flags;
  }

  @Data
  public static class DiscordApplication
  {
    private String id;
    private String name;
    private String icon;
    private String description;
    private Boolean hook;
    private Boolean bot_public;
    private Boolean bot_require_code_grant;
    private String verify_key;
  }
}
