package com.ethernity.kamabot.discord.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.reactive.function.BodyInserters;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class DiscordApiOAuthAccessRequest
{
  private final String client_id;
  private final String client_secret;
  private final String code;
  private final String redirect_uri;
  private String grant_type = "authorization_code";

  public BodyInserters.FormInserter<String> toFormInserter()
  {
    return BodyInserters
        .fromFormData("client_id", client_id)
        .with("client_secret", client_secret)
        .with("grant_type", grant_type)
        .with("code", code)
        .with("redirect_uri", redirect_uri);
  }

  public String toFormData()
  {
    return "client_id=%s&client_secret=%s&grant_type=%s&code=%s&redirect_uri=%s".formatted(client_id, client_secret, grant_type, code, URLEncoder.encode(redirect_uri, StandardCharsets.UTF_8));
  }
}
