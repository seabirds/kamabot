package com.ethernity.kamabot.discord;

import com.ethernity.kamabot.command.CommandRegistry;
import lombok.SneakyThrows;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class DiscordCommandsHandler implements MessageCreateListener
{

  Logger logger = LoggerFactory.getLogger(DiscordCommandsHandler.class);

  @Autowired
  private DiscordService discordService;
  @Autowired
  private DiscordIDs discordIDs;

  @Autowired
  private CommandRegistry commandRegistry;

  @SneakyThrows
  @Override
  public void onMessageCreate(MessageCreateEvent event)
  {

    TextChannel channel = event.getChannel();

    // Only react to message with content
    if (event.getMessage().getContent().length() == 0) {
      return;
    }

    String message = event.getMessage().getContent();

    if (!message.startsWith(discordIDs.commandPrefix)) {
      return;
    }

    // Find and execute the right command
    logger.info("Detecting command " + message);
    commandRegistry.execute(channel.getIdAsString(), message.substring(1).split(" ")[0].toUpperCase(), message);
  }
}
