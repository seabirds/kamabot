package com.ethernity.kamabot.discord;

import com.ethernity.kamabot.dto.MessageReference;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.Channel;
import org.javacord.api.entity.channel.PrivateChannel;
import org.javacord.api.entity.channel.ServerVoiceChannel;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageFlag;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.interaction.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class DiscordService
{
  @Autowired
  private DiscordIDs discordIDs;

  @Autowired
  private DiscordApi client;

  public Message getMessage(MessageReference message)
  {
    return this.getMessage(message.getMessageId(), message.getChannelId());
  }

  public Message getMessage(String messageId, TextChannel channel)
  {
    try {
      return client.getMessageById(messageId, channel).get();
    } catch (Exception e) {
      log.error("Cannot find message", e);
      return null;
    }
  }

  public Message getMessage(String messageId, String channelId)
  {
    try {
      return client.getMessageById(messageId, getTextChannel(channelId)).get();
    } catch (Exception e) {
      log.error("Cannot find message", e);
      return null;
    }
  }

  public void deleteMessage(MessageReference messageReference)
  {
    this.deleteMessage(messageReference.getMessageId(), messageReference.getChannelId());
  }

  public void deleteMessage(String messageId, String channelId)
  {
    try {
      TextChannel textChannel = getTextChannel(channelId);
      Message message = getMessage(messageId, textChannel);
      message.delete().get();
    } catch (Exception e) {
      log.error("Cannot delete message " + messageId, e);
    }
  }

  public TextChannel getTextChannel(Long channelId)
  {
    try {
      return client.getTextChannelById(channelId).get();
    } catch (Exception e) {
      log.error("Cannot find message", e);
      return null;
    }
  }

  public TextChannel getTextChannel(String channelId)
  {
    try {
      return client.getTextChannelById(channelId).get();
    } catch (Exception e) {
      log.error("Cannot find message", e);
      return null;
    }
  }

  public ServerVoiceChannel getVoiceChannel(long channelId)
  {
    try {
      return client.getServerVoiceChannelById(channelId).get();
    } catch (Exception e) {
      log.error("Cannot find message", e);
      return null;
    }
  }

  public Message getMessageOnMP(long messageId, long userId)
  {
    try {
      User user = client.getUserById(userId).get();
      PrivateChannel channel = user.openPrivateChannel().get();
      return channel.getMessageById(messageId).get();
    } catch (Exception e) {
      log.error("Cannot find message or private user channel", e);
      return null;
    }
  }

  public User getUser(long userId)
  {
    return getUser(String.valueOf(userId));
  }

  public User getUser(String userId)
  {
    try {
      return client.getUserById(userId).get();
    } catch (Exception e) {
      log.error("Cannot find user", e);
      return null;
    }
  }

  public String getUserNickname(Long userId)
  {
    return getUserNickname(String.valueOf(userId));
  }

  public String getUserNickname(String userId)
  {
    try {
      return getServer().getDisplayName(getUser(userId));
    } catch (Exception e) {
      log.error("Cannot find user", e);
      return null;
    }
  }

  public Message sendMessage(String channelId, String message)
  {
    return client.getTextChannelById(channelId).map(c -> sendMessage(c, message)).orElse(null);
  }

  public Message sendMessage(TextChannel channel, String message)
  {
    try {
      return channel.sendMessage(message).get();
    } catch (Exception e) {
      return null;
    }
  }

  public Message sendFile(TextChannel channel, File file)
  {
    try {
      return channel.sendMessage(file).get();
    } catch (Exception e) {
      return null;
    }
  }

  public CompletableFuture<Message> sendMessageAsync(TextChannel channel, String message)
  {
    return channel.sendMessage(message);
  }

  public Message sendMessageWithEmbed(String channelId, String title, Map<String, String> fields)
  {
    EmbedBuilder embedBuilder = getEmbedBuilder(title, fields);

    return client.getChannelById(channelId).flatMap(Channel::asTextChannel).map(c ->
    {
      try {
        return c.sendMessage(embedBuilder).get();
      } catch (Exception e) {
        log.error("Error sending message to Discord", e);
        return null;
      }
    }).orElse(null);
  }

  public Message sendPrivateMessage(long userId, String title, Map<String, String> fields)
  {
    try {
      return client.getUserById(userId).get().sendMessage(getEmbedBuilder(title, fields)).get();
    } catch (Exception e) {
      return null;
    }
  }

  public Message sendEmbed(String channelId, EmbedBuilder embedBuilder)
  {
    return client.getChannelById(channelId).flatMap(Channel::asTextChannel).map(c ->
        this.sendEmbed(c, embedBuilder)).orElse(null);
  }

  public Message sendEmbed(TextChannel textChannel, EmbedBuilder embedBuilder)
  {
    try {
      return textChannel.sendMessage(embedBuilder).get();
    } catch (Exception e) {
      log.error("Error sending embed to Discord", e);
      return null;
    }
  }

  public Message sendEmbedAsPM(long personId, EmbedBuilder embedBuilder)
  {
    try {
      User user = client.getUserById(personId).get();
      return user.sendMessage(embedBuilder).get();
    } catch (Exception e) {
      log.error("Error sending private message to Discord to user " + personId, e);
      return null;
    }
  }

  public Server getServer()
  {
    try {
      return client.getServerById(discordIDs.serverId).get();
    } catch (Exception e) {
      log.error("Cannot find server", e);
      return null;
    }
  }


  public void editMessage(Message message, String title, Map<String, String> fields)
  {
    EmbedBuilder embedBuilder = new EmbedBuilder()
        .setTitle(title);
    fields.keySet().forEach(k -> embedBuilder.addField(k, fields.get(k)));

    message.edit(embedBuilder);
  }

  public SlashCommand registerSlashCommand(String commandName, String commandDescription, List<SlashCommandOption> options)
  {
    return SlashCommand.with(commandName, commandDescription, options).createForServer(getServer()).join();
  }

  public UserContextMenu registerUserContextMenuCommand(String command, String commandDescription)
  {
    return UserContextMenu.with(command).setDescription(commandDescription).createForServer(getServer()).join();
  }

  public MessageContextMenu registerMessageContextMenuCommand(String command)
  {
//    return MessageContextMenu.with(command, commandDescription).createForServer(getServer()).join();
    return MessageContextMenu.with(command).setDescription("").createForServer(getServer()).join();
  }

  private EmbedBuilder getEmbedBuilder(String title, Map<String, String> fields)
  {
    EmbedBuilder embedBuilder = new EmbedBuilder()
        .setTitle(title);
    fields.keySet().forEach(k -> embedBuilder.addField(k, fields.get(k)));
    return embedBuilder;
  }

  public void respondSimpleMessage(InteractionBase interactionBase, String message)
  {
    interactionBase.createImmediateResponder()
        .setContent(message)
        .respond();
  }

  public void respondEphemeralMessage(InteractionBase interactionBase, String message)
  {
    interactionBase.createImmediateResponder()
        .setContent(message)
        .setFlags(MessageFlag.EPHEMERAL)
        .respond();
  }
}