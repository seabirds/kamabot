package com.ethernity.kamabot.discord;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DiscordReadyHandler
{

  @Autowired
  private DiscordIDs discordIDs;
  @Autowired
  private DiscordService discordService;

  public void onServerJoin()
  {

  }
}
