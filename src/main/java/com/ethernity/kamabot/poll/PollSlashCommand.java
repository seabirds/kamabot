package com.ethernity.kamabot.poll;

import com.ethernity.kamabot.constants.Emojis;
import com.ethernity.kamabot.dto.MessageReference;
import com.ethernity.kamabot.slashcommand.SlashCommand;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.entity.message.Message;
import org.javacord.api.interaction.SlashCommandInteraction;
import org.javacord.api.interaction.SlashCommandInteractionOption;
import org.javacord.api.interaction.SlashCommandOption;
import org.javacord.api.interaction.SlashCommandOptionType;
import org.javacord.api.interaction.callback.InteractionOriginalResponseUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class PollSlashCommand extends SlashCommand
{
  private final static String SUBCOMMAND_LOOT = "loot";

  @Autowired
  protected PollReactionsWatcher pollReactionsWatcher;
  @Autowired
  protected PollEmbedMapper pollEmbedMapper;

  @Autowired
  protected PollRepository pollRepository;

  @Override
  public void execute(String channelId, SlashCommandInteraction slashCommandInteraction) throws ExecutionException, InterruptedException
  {
    String question = slashCommandInteraction.getOptionByName("Question").flatMap(SlashCommandInteractionOption::getStringValue).get();

    String reponse1 = slashCommandInteraction.getOptionByName("Réponse1").flatMap(SlashCommandInteractionOption::getStringValue).get();
    String reponse2 = slashCommandInteraction.getOptionByName("Réponse2").flatMap(SlashCommandInteractionOption::getStringValue).get();
    String reponse3 = slashCommandInteraction.getOptionByName("Réponse3").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse4 = slashCommandInteraction.getOptionByName("Réponse4").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse5 = slashCommandInteraction.getOptionByName("Réponse5").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse6 = slashCommandInteraction.getOptionByName("Réponse6").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse7 = slashCommandInteraction.getOptionByName("Réponse7").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse8 = slashCommandInteraction.getOptionByName("Réponse8").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);
    String reponse9 = slashCommandInteraction.getOptionByName("Réponse9").flatMap(SlashCommandInteractionOption::getStringValue).orElse(null);

    // Preparing the options / answers lists
    List<String> options = Stream.of(reponse1, reponse2, reponse3, reponse4, reponse5, reponse6, reponse7, reponse8, reponse9).filter(Objects::nonNull).collect(Collectors.toList());

    // Sending the poll, storing the discord messaage object into it
    Poll poll = new Poll(question, options);
    // Message message = discordService.sendEmbed(slashCommandInteraction.getChannel().get().getId(), poll.toEmbed());

    // Watching reactions

    InteractionOriginalResponseUpdater responseUpdater = slashCommandInteraction.createImmediateResponder()
        .addEmbed(pollEmbedMapper.toEmbed(poll))
        .respond()
        .get();

    Message message = responseUpdater.update().get();

    // Adding the initial set of reactions to the message
    for (int i = 0, pOptionsSize = options.size(); i < pOptionsSize; i++) {
      message.addReaction(Emojis.REACTION_EMOJIS[i]).get();
    }
    message.addReaction("❌");

    poll.setMessageReference(new MessageReference(message.getIdAsString(), slashCommandInteraction.getChannel().get().getIdAsString()));

    pollRepository.save(poll);

    pollReactionsWatcher.watch(poll, message);
  }

  @Override
  public String getCommandName()
  {
    return "sondage";
  }

  @Override
  public String getDescription()
  {
    return "Permet de créer un sondage";
  }

  @Override
  public List<SlashCommandOption> getOptions()
  {
    return Arrays.asList(
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Question", "La question à poser", true),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse1", "Première réponse", true),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse2", "2eme réponse", true),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse3", "3ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse4", "4ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse5", "5ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse6", "6ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse7", "7ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse8", "8ème réponse"),
        SlashCommandOption.create(SlashCommandOptionType.STRING, "Réponse9", "9ème réponse")
    );
  }
}
