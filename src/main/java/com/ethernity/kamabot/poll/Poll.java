package com.ethernity.kamabot.poll;

import com.ethernity.kamabot.dto.FirebaseData;
import com.ethernity.kamabot.dto.MessageReference;
import com.google.firebase.database.Exclude;
import lombok.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Poll implements FirebaseData
{
  private String id;

  private MessageReference messageReference;

  private String title;
  private List<String> options;
  private Map<String, List<Long>> answers;

  private List<Long> whiteAnswers;
  private Long creationTimestamp;

  public Poll(String title, List<String> options)
  {
    this.title = title;

    this.options = options;

    this.answers = new HashMap<>();
    for (String option : options) {
      answers.put(option, new ArrayList<>());
    }

    this.creationTimestamp = Timestamp.valueOf(LocalDateTime.now()).getTime();
  }

  public LocalDateTime creationDate()
  {
    return LocalDateTime.ofInstant(Instant.ofEpochMilli(this.creationTimestamp), TimeZone.getDefault().toZoneId());
  }

  public LocalDateTime expirationDate()
  {
    return this.creationDate().plus(2, ChronoUnit.WEEKS);
  }

  @Exclude
  public boolean isExpired()
  {
    return this.expirationDate().isBefore(LocalDateTime.now());
  }
}
