package com.ethernity.kamabot.poll;

import com.ethernity.kamabot.constants.Emojis;
import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import org.javacord.api.entity.emoji.Emoji;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.Reaction;
import org.javacord.api.entity.user.User;
import org.javacord.api.listener.message.reaction.ReactionAddListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.ethernity.kamabot.poll.PollConstants.POLL_WHITE_ANSWER_EMOJI;

@Service
public class PollReactionsWatcher
{
  Logger logger = LoggerFactory.getLogger(PollReactionsWatcher.class);

  private Random random = new Random();

  private Map<String, ReactionAddListener> listenerMap = new HashMap<>();
  private Map<String, Message> messageMap = new HashMap<>();

  @Autowired
  private PollRepository pollRepository;
  @Autowired
  private PollEmbedMapper pollEmbedMapper;

  @Autowired
  private DiscordService discordService;
  @Autowired
  private DiscordIDs discordIDs;

  public void recoverMissedVotes(Poll poll, Message message)
  {
    List<Reaction> reactions = message.getReactions();
    reactions.forEach(reaction ->
    {
      if (reaction.getCount() > 1) {
        try {
          List<User> votingUsers = reaction.getUsers().get().stream().filter(user -> !user.isBot()).collect(Collectors.toList());

          votingUsers.forEach(user ->
          {
            voteToPoll(poll, message, reaction.getEmoji(), user.getId());

            reaction.removeUser(user);
          });
        } catch (InterruptedException | ExecutionException e) {
          logger.error("Unable to recover old forgotten votes", e);
        }
      }
    });
  }

  public void voteToPoll(Poll poll, Message message, Emoji emoji, long userId)
  {

    logger.info("Detecting reaction from " + userId);

    // Getting the reacting username
    User user = discordService.getUser(userId);
    String userName = user.getDisplayName(discordService.getServer());

    // Getting the reaction emoji
    String unicodeEmoji = emoji.asUnicodeEmoji().get();
    if (unicodeEmoji.equals(POLL_WHITE_ANSWER_EMOJI)) {
      if (poll.getWhiteAnswers() == null) {
        poll.setWhiteAnswers(new ArrayList<>());
      }

      // Toggle presence of the username
      if (poll.getWhiteAnswers().contains(userId)) {
        poll.getWhiteAnswers().remove(userId);
      } else {
        poll.getWhiteAnswers().add(userId);

        poll.getAnswers().values().forEach(answers -> answers.remove(userId));
      }

    } else {

      int answerIndex = findEmojiIndex(unicodeEmoji, poll.getOptions().size());

      // Can only act upon recognized reaction
      if (answerIndex > -1 && poll.getOptions().size() > answerIndex) {
        String option = poll.getOptions().get(answerIndex);

        List<Long> answersByUsersId = poll.getAnswers() != null ? poll.getAnswers().get(option) : null;
        if (answersByUsersId == null) {
          answersByUsersId = new ArrayList<>();
        }

        // Toggle presence of the username
        if (answersByUsersId.contains(userId)) {
          answersByUsersId.remove(userId);
        } else {
          answersByUsersId.add(userId);
          if (poll.getWhiteAnswers() != null) {
            poll.getWhiteAnswers().remove(userId);
          }
        }

        if (poll.getAnswers() == null) {
          poll.setAnswers(new HashMap<>());
        }
        poll.getAnswers().put(option, answersByUsersId);
      } else {
        logger.debug("Incoherent reaction, possibly trolling by " + userName + " ignoring it. Consider trashing the dumb guy.");
      }
    }

    pollRepository.save(poll);
    message.edit(pollEmbedMapper.toEmbed(poll));
  }

  public void watch(Poll poll, Message message)
  {
    final ReactionAddListener listener = reactionAddEvent ->
    {
      // Exclude bot itself
      if (reactionAddEvent.getUserId() == discordIDs.botId) {
        return;
      }
      voteToPoll(poll, message, reactionAddEvent.getEmoji(), reactionAddEvent.getUserId());

      // Removing the reaction to keep things clean
      try {
        reactionAddEvent.removeReaction().get();
      } catch (Exception e) {
        e.printStackTrace();
      }
    };

    message.addReactionAddListener(listener);
    message.addMessageDeleteListener((event) ->
    {
      message.removeListener(ReactionAddListener.class, listener);
      pollRepository.delete(poll);
    });
    this.listenerMap.put(poll.getId(), listener);
    this.messageMap.put(poll.getId(), message);
  }

  public void unwatch(Poll poll)
  {
    ReactionAddListener listener = this.listenerMap.get(poll.getId());
    Message message = this.messageMap.get(poll.getId());

    if (message != null) {
      message.removeListener(ReactionAddListener.class, listener);
      message.edit(pollEmbedMapper.toEmbed(poll));
      message.removeAllReactions();
    }
    pollRepository.delete(poll);
  }

  public int findEmojiIndex(String emoji, int answersSize)
  {
    String[] reaction_emojis = Emojis.REACTION_EMOJIS;
    if (emoji.equals("❌")) {
      return answersSize - 1;
    }
    for (int i = 0, reaction_emojisLength = reaction_emojis.length; i < reaction_emojisLength; i++) {
      String reactionEmoji = reaction_emojis[i];
      if (reactionEmoji.equals(emoji))
        return i;

    }
    return -1;
  }
}
