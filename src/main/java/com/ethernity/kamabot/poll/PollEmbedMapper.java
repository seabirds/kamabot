package com.ethernity.kamabot.poll;

import com.ethernity.kamabot.constants.Emojis;
import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.utils.DateUtils;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

import static com.ethernity.kamabot.poll.PollConstants.POLL_WHITE_ANSWER_EMOJI;
import static com.ethernity.kamabot.poll.PollConstants.POLL_WHITE_ANSWER_OPTION;

@Component
public class PollEmbedMapper
{
  @Autowired
  private DiscordService discordService;

  private void toEmbedLine(EmbedBuilder embed, String option, List<String> selectedByUsers, String answerEmoji)
  {
    String answersAsString;
    int answerCount = 0;
    if (selectedByUsers == null || selectedByUsers.size() == 0) {
      answersAsString = "\u200b";
    } else {
      answerCount = selectedByUsers.size();
      answersAsString = String.join("\n", selectedByUsers);
    }

    embed.addInlineField(answerEmoji + "   " + option + " (" + answerCount + ")", answersAsString);
  }

  public EmbedBuilder toEmbed(Poll poll)
  {
    EmbedBuilder embed = new EmbedBuilder();
    embed.setTitle(poll.getTitle());
    for (int i = 0, optionsSize = poll.getOptions().size(); i < optionsSize; i++) {
      String option = poll.getOptions().get(i);
      List<Long> selectedByUserIdAnswers = poll.getAnswers().get(option);
      List<String> selectedByUserAnswers = selectedByUserIdAnswers.stream().map(userId -> discordService.getUserNickname(userId)).toList();
      String answerEmoji = Emojis.REACTION_EMOJIS[i];

      toEmbedLine(embed, option, selectedByUserAnswers, answerEmoji);
    }

    List<String> whiteAnswersSelectedByUser;
    if (poll.getWhiteAnswers() != null) {
      whiteAnswersSelectedByUser = poll.getWhiteAnswers().stream().map(userId -> discordService.getUserNickname(userId)).toList();
    } else {
      whiteAnswersSelectedByUser = Collections.emptyList();
    }
    toEmbedLine(embed, POLL_WHITE_ANSWER_OPTION, whiteAnswersSelectedByUser, POLL_WHITE_ANSWER_EMOJI);

    // TODO: remove ads better
    // embed.setFooter(AdsConstants.ADS[this.adId]);
    if (poll.isExpired()) {
      embed.setFooter("Sondage terminé");
    } else {
      embed.setFooter("Expiration du sondage le " + poll.expirationDate().format(DateUtils.dateFormatter));
    }

    return embed;
  }
}
