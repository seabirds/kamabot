package com.ethernity.kamabot.poll;

import com.ethernity.kamabot.dto.FirebaseRepository;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Component;

@Component
public class PollRepository extends FirebaseRepository<Poll>
{
  public PollRepository(FirebaseDatabase firebaseDatabase)
  {
    super(firebaseDatabase, "polls");
  }
}
