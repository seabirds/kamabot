package com.ethernity.kamabot;

import com.ethernity.kamabot.command.CommandRegistry;
import com.ethernity.kamabot.discord.DiscordCommandsHandler;
import com.ethernity.kamabot.discord.DiscordReadyHandler;
import com.ethernity.kamabot.discord.DiscordService;
import com.ethernity.kamabot.slashcommand.SlashCommandRegistry;
import com.ethernity.kamabot.slashcommand.SlashCommandsDispatcher;
import com.ethernity.kamabot.watcher.WatcherRegistry;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.DiscordApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by jimmy on 08/07/2017.
 */
@SpringBootApplication
@EnableScheduling
@Slf4j
public class Application implements ApplicationRunner
{
  @Autowired
  private DiscordCommandsHandler commandsHandler;
  @Autowired
  private DiscordReadyHandler readyHandler;

  @Autowired
  private CommandRegistry commandRegistry;
  @Autowired
  private SlashCommandRegistry slashCommandRegistry;
  @Autowired
  private WatcherRegistry watcherRegistry;
  @Autowired
  private SlashCommandsDispatcher slashCommandsDispatcher;

  @Autowired
  private DiscordApi discordApi;
  @Autowired
  private DiscordService discordService;

  public static void main(String[] args)
  {
    SpringApplication.run(Application.class, args);
  }

  @Override
  public void run(ApplicationArguments applicationArguments) throws Exception
  {
    readyHandler.onServerJoin();

    discordApi.addMessageCreateListener(commandsHandler);

    commandRegistry.setUp();

    slashCommandRegistry.registerCommandsOnDiscord();

    discordApi.addModalSubmitListener(slashCommandsDispatcher);
    discordApi.addSlashCommandCreateListener(slashCommandsDispatcher);
    discordApi.addMessageContextMenuCommandListener(slashCommandsDispatcher);

    watcherRegistry.startWatching();
  }
}
