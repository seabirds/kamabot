package com.ethernity.kamabot.constants;

public class Emojis
{
  public static String[] REACTION_EMOJIS = new String[]{
      "1️⃣",
      "2️⃣",
      "3️⃣",
      "4️⃣",
      "5️⃣",
      "6️⃣",
      "7️⃣",
      "8️⃣",
      "9️⃣",
  };
}
