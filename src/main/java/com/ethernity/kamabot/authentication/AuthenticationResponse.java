package com.ethernity.kamabot.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AuthenticationResponse
{
  private LocalDateTime discordExpiresAt;
  private String discordRefreshToken;
  private String discordToken;
  private String firebaseToken;
  private LocalDateTime firebaseExpiresAt;
  private String userId;

  public boolean isDiscordTokenExpired()
  {
    return discordExpiresAt.isBefore(LocalDateTime.now());
  }

  public boolean isFirebaseTokenExpired()
  {
    return firebaseExpiresAt.isBefore(LocalDateTime.now());
  }
}
