package com.ethernity.kamabot.authentication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
@Slf4j
public class AuthenticationController
{
  @Autowired
  private AuthenticationService authenticationService;

  @PostMapping("")
  public AuthenticationResponse login(@RequestBody AuthenticationRequest authenticationRequest)
  {
    return this.authenticationService.login(authenticationRequest.getDiscordAccessCode());
  }

  @PostMapping("refresh")
  public AuthenticationResponse refresh(@RequestBody AuthenticationResponse authData)
  {
    return this.authenticationService.refresh(authData);
  }
}
