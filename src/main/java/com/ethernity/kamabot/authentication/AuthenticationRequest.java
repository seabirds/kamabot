package com.ethernity.kamabot.authentication;

import lombok.Data;

@Data
public class AuthenticationRequest
{
  private String discordAccessCode;
}
