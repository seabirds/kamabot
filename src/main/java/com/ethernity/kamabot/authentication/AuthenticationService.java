package com.ethernity.kamabot.authentication;

import com.ethernity.kamabot.discord.api.DiscordApiOAuthResponse;
import com.ethernity.kamabot.discord.api.DiscordApiUser;
import com.ethernity.kamabot.discord.api.DiscordApiWrapper;
import com.google.firebase.auth.FirebaseAuth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class AuthenticationService
{
  @Autowired
  private DiscordApiWrapper discordApiWrapper;

  @Autowired
  private FirebaseAuth firebaseAuth;

  public AuthenticationResponse login(String discordAccessCode)
  {
    try {
      DiscordApiOAuthResponse discordOAuthResponse = discordApiWrapper.requireAccessToken(discordAccessCode);
      DiscordApiUser user = discordApiWrapper.getUserByToken(discordOAuthResponse.getAccess_token());
      if (user != null && user.getUser() != null) {
        String firebaseToken = firebaseAuth.createCustomToken(user.getUser().getId());
        LocalDateTime discordOAuthExpiryDate = LocalDateTime.now().plus(discordOAuthResponse.getExpires_in(), ChronoUnit.SECONDS);
        LocalDateTime firebaseJwtExpiryDate = LocalDateTime.now().plus(1, ChronoUnit.HOURS);
        return new AuthenticationResponse(discordOAuthExpiryDate, discordOAuthResponse.getRefresh_token(), discordOAuthResponse.getAccess_token(), firebaseToken, firebaseJwtExpiryDate, user.getUser().getId());
      } else {
        throw new RuntimeException("Login failure");
      }
    } catch (Exception e) {
      log.error("Issue when logging in", e);
      throw new RuntimeException(e);
    }
  }

  public AuthenticationResponse refresh(AuthenticationResponse authData)
  {

    try {
      if (authData.isDiscordTokenExpired()) {
        DiscordApiOAuthResponse discordApiOAuthResponse = discordApiWrapper.refreshAccessToken(authData.getDiscordRefreshToken());
        LocalDateTime discordOAuthExpiryDate = LocalDateTime.now().plus(discordApiOAuthResponse.getExpires_in(), ChronoUnit.SECONDS);
        authData.setDiscordExpiresAt(discordOAuthExpiryDate);
        authData.setDiscordToken(discordApiOAuthResponse.getAccess_token());
        authData.setDiscordRefreshToken(discordApiOAuthResponse.getRefresh_token());
      }

      if (authData.isFirebaseTokenExpired()) {
        authData.setFirebaseToken(firebaseAuth.createCustomToken(authData.getUserId()));
        authData.setFirebaseExpiresAt(LocalDateTime.now().plus(1, ChronoUnit.HOURS));
      }

      return authData;
    } catch (Exception e) {
      log.error("Issue when refreshing tokens", e);
      return null;
    }
  }
}
