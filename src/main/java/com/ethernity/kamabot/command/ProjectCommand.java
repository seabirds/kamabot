//package com.ethernity.kamabot.command;
//
//import com.ethernity.kamabot.model.Project;
//import com.ethernity.kamabot.model.ProjectRepository;
//import com.ethernity.kamabot.model.Task;
//import com.ethernity.kamabot.model.TaskRepository;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.TaskScheduler;
//import org.springframework.stereotype.Component;
//import sx.blah.discord.api.internal.json.objects.EmbedObject;
//import sx.blah.discord.handle.impl.obj.Embed;
//
//import java.util.List;
//import java.util.UUID;
//
//@Component
//@Slf4j
//public class ProjectCommand extends Command {
//
//    @Autowired
//    private ProjectRepository projectRepository;
//    @Autowired
//    private TaskRepository taskRepository;
//
//    /**
//     * Command parameters and parsers
//     */
//    @Data
//    private static class Parameters {
//        public enum Operation {
//            ADD,
//            PROGRESS,
//            STATUS,
//            LIST,
//            REMOVE
//        }
//
//        private Operation operation;
//        private String projectName;
//        private String itemName;
//        private int amount;
//
//        public static Parameters parseParameters(String cmdText) {
//            String[] cmd = cmdText.split("\"");
//
//            Parameters p = new Parameters();
//            p.setProjectName(cmd[1].toUpperCase());
//            p.setOperation(Operation.valueOf(cmd[2].trim().toUpperCase()));
//
//            // Optional parameters
//            if(cmd.length > 3) {
//                p.setItemName(cmd[3].toUpperCase());
//            }
//            if(cmd.length > 4) {
//                p.setAmount(Integer.parseInt(cmd[4].trim()));
//            }
//
//            return p;
//        }
//    }
//
//    /**
//     * Command execution
//     */
//    @Override
//    public void execute(String cmdText) {
//        try {
//            Parameters parameters = Parameters.parseParameters(cmdText);
//            switch(parameters.getOperation())
//            {
//                case ADD:
//                    handleAddOperation(parameters);
//                    break;
//                case PROGRESS:
//                    handleProgressOperation(parameters);
//                    break;
//                case STATUS:
//                    handleStatusOperation(parameters);
//                    break;
//                case LIST:
//                    handleListOperation(parameters);
//                    break;
//                case REMOVE:
//                    handleRemoveOperation(parameters);
//                    break;
//                default:
//                    throw new Exception("Unrecognized operation");
//            }
//        }
//        catch (CommandException e) {
//            log.error("Command project error", e);
//            discordService.sendMessage(discordChannels.botChannel, "Bad command");
//            discordService.sendMessage(discordChannels.botChannel, "!project <NAME> <add/progress/status/remove> <ITEM> <AMOUNT>");
//
//        }
//        catch(Exception e) {
//            log.error("Error executing the command", e);
//            discordService.sendMessage(discordChannels.botChannel, "Command execution error, please try again later");
//        }
//    }
//
//    /**
//     * Handle a list of project operation
//     */
//    private void handleListOperation(Parameters parameters) {
//
//    }
//
//    /**
//     * Print the project status
//     */
//    private void handleStatusOperation(Parameters parameters) {
//        Project project = projectRepository.findOne(parameters.getProjectName());
//
//        // Splitting in multiple embedded
//        int nbrMessages = (project.getTasks().size() / 25)+1;
//        EmbedObject[] messages = new EmbedObject[nbrMessages];
//
//        if (project.getTasks().size() == 0) {
//            discordService.sendBotChannel("No content in this project");
//            return;
//        }
//
//        discordService.sendBotChannel("Project " + project.getName() + "   -----   " + project.getPercentage() + " %");
//
//        for(int j = 0; j < nbrMessages; j++) {
//            List<Task> pageTask = project.getTasks().subList(j*25, Math.min(project.getTasks().size(), (j+1)*25));
//
//            messages[j] = new EmbedObject();
//            messages[j].fields = new EmbedObject.EmbedFieldObject[pageTask.size()];
//
//            for (int i = 0; i < pageTask.size(); i++) {
//                Task t = pageTask.get(i);
//                messages[j].fields[i] = new EmbedObject.EmbedFieldObject(t.getName(), t.getAmount() + "/" + t.getTotalAmount(), false);
//            }
//
//            discordService.sendBotChannel(messages[j]);
//        }
//    }
//
//    /**
//     * Remove a project
//     */
//    private void handleRemoveOperation(Parameters parameters) {
//
//    }
//
//    /**
//     * Makes a project item progress
//     */
//    private void handleProgressOperation(Parameters parameters) {
//        Task t = taskRepository.getTaskByNameAndProject(parameters.getItemName(), parameters.getProjectName());
//        if(t == null) {
//            discordService.sendBotChannel("Item " + t.getName() + " unknown");
//        }
//
//        t.setAmount(t.getAmount() + parameters.getAmount());
//        if(t.getAmount() > t.getTotalAmount()) {
//            t.setAmount(t.getTotalAmount());
//        }
//        taskRepository.save(t);
//
//        discordService.sendBotChannel("Added progress of " + parameters.getAmount() + " to " + parameters.getItemName());
//    }
//
//    private void handleAddOperation(Parameters parameters) {
//        Task t = taskRepository.getTaskByNameAndProject(parameters.getItemName(), parameters.getProjectName());
//        if(t != null) {
//            discordService.sendMessage(discordChannels.botChannel, "This item is already in the project");
//            return;
//        }
//        Project project = projectRepository.findOne(parameters.getProjectName());
//        if(project == null) {
//            project = new Project();
//            project.setName(parameters.getProjectName());
//            project = projectRepository.save(project);
//        }
//
//        t = new Task();
//        t.setId(project.getName() + "_" + parameters.getItemName());
//        t.setProject(project);
//        t.setName(parameters.getItemName().toUpperCase());
//        t.setAmount(0);
//        t.setTotalAmount(parameters.getAmount());
//        t = taskRepository.save(t);
//
//        discordService.sendMessage(discordChannels.botChannel, "Added " + parameters.getItemName() + "(" + parameters.getAmount() + ") to project " + parameters.getProjectName());
//    }
//
//    @Override
//    public String getCommandName() {
//        return "PROJECT";
//    }
//}
