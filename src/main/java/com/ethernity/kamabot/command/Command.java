package com.ethernity.kamabot.command;

import com.ethernity.kamabot.discord.DiscordIDs;
import com.ethernity.kamabot.discord.DiscordService;
import org.javacord.api.entity.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutionException;

public abstract class Command
{
  @Autowired
  protected DiscordService discordService;
  @Autowired
  protected DiscordIDs discordIDs;

  public abstract void execute(String channelId, String cmdText) throws ExecutionException, InterruptedException;

  public void execute(Channel channel, String cmdText) throws ExecutionException, InterruptedException
  {
    execute(channel.getIdAsString(), cmdText);
  }

  public void setUp()
  {
    // No operation by default
  }

  public abstract String getCommandName();

  public void cleanUp()
  {
    // No operation maybe
  }
}
