package com.ethernity.kamabot.command;

import com.ethernity.kamabot.discord.DiscordIDs;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
public class CommandRegistry
{

  private final Map<String, Command> commands = new HashMap<>();

  private DiscordIDs discordIDs;

  public CommandRegistry(DiscordIDs discordIDs)
  {
    this.discordIDs = discordIDs;
  }

  public void register(Command cmd)
  {
    commands.put(cmd.getCommandName(), cmd);
  }

  public void setUp()
  {
    commands.values().forEach(Command::setUp);
  }

  public void execute(String channelId, String cmdName, String fullMessage) throws ExecutionException, InterruptedException
  {
    Command command = commands.get(cmdName);
    if (command != null) {
      command.execute(channelId, fullMessage);
    } else {
      log.info("Command unrecognized, ignoring");
    }
  }

  public void cleanUp()
  {
    commands.values().forEach(Command::cleanUp);
  }
}
