//package com.ethernity.kamabot.fflogs;
//
//import io.github.bonigarcia.wdm.WebDriverManager;
//import lombok.extern.slf4j.Slf4j;
//import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.remote.RemoteWebDriver;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Component;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//@Slf4j
//@Lazy
//@Component
//public class FFLogsDumper
//{
//  @Autowired
//  private Environment env;
//
//  private WebDriver driver;
//
//  private Pattern fflogsUrlPattern = Pattern.compile("https:\\/\\/fr.fflogs.com\\/reports\\/(.*)\\/");
//
//  public FFLogsDumper()
//  {
//    WebDriverManager.firefoxdriver().setup();
//    FirefoxOptions options = new FirefoxOptions();
//    options.addArguments("-headless", "-disable-gpu", "--width=800", "--height=478", "-ignore-certificate-errors", "-hide-scrollbars");
//    try {
//      driver = new RemoteWebDriver(new URL("http://192.168.1.97:4444/wd/hub"), options);
//    } catch (MalformedURLException e) {
//      throw new RuntimeException(e);
//    }
//    driver.close();
//  }
//
//  public String extractLogIdFromUrl(String url)
//  {
//    Matcher matcher = fflogsUrlPattern.matcher(url);
//    if (matcher.find()) {
//      return matcher.group(1);
//    } else {
//      return null;
//    }
//  }
//
//  public File capture(String fflogsId) throws IOException, InterruptedException
//  {
//    this.driver.get("https://fr.fflogs.com/reports/" + fflogsId + "/#fight=last&type=damage-done");
//    Thread.sleep(500);
//    this.executePurgeJavascript();
//    return ((TakesScreenshot) this.driver).getScreenshotAs(OutputType.FILE);
//  }
//
//  private void executePurgeJavascript()
//  {
////    Dimension d = new Dimension(800, 478);
////    driver.manage().window().setSize(d);
//    JavascriptExecutor js = (JavascriptExecutor) driver;
//    js.executeScript("let noElement = {remove: () => {}};" +
//        "document.getElementsByTagName('body')[0].style.padding = '0';" +
//        "document.getElementsByTagName('body')[0].style.overflow = 'none';" +
//        "(document.getElementById('qc-cmp2-container') || noElement).remove();" +
//        "(document.getElementById('header-container') || noElement).remove();" +
//        "(document.getElementById('top-banner') || noElement).remove();" +
//        "(document.getElementById('report-info-bar') || noElement).remove();" +
//        "(document.getElementById('filter-type-tabs-wrapper') || noElement).remove();" +
//        "(document.getElementById('top-level-view-tabs-container') || noElement).remove();" +
//        "(document.getElementById('filter-subtype-strip') || noElement).remove();" +
//        "(document.getElementById('graph-container') || noElement).remove();" +
//        "(document.getElementById('filter-type-menubar') || noElement).remove();" +
//        "Array.from(document.getElementsByClassName('cc-window cc-banner cc-type-info')).forEach(e => e.remove());");
//  }
//}
