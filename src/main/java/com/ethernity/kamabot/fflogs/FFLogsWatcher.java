//package com.ethernity.kamabot.fflogs;
//
//import com.ethernity.kamabot.watcher.Watcher;
//import lombok.extern.slf4j.Slf4j;
//import org.javacord.api.entity.message.Message;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.io.File;
//import java.io.IOException;
//
//@Slf4j
//@Component
//public class FFLogsWatcher extends Watcher
//{
//  @Autowired
//  private FFLogsDumper ffLogsDumper;
//
//  @Override
//  public String getName()
//  {
//    return "fflogs-watcher";
//  }
//
//  @Override
//  public void reactToMessage(Message message)
//  {
//    try {
//      String fflogId = ffLogsDumper.extractLogIdFromUrl(message.getContent());
//      if (fflogId != null && message.getContent().startsWith("http")) {
//        log.info("Detecting an FFLog message in the right channel, extracting log " + fflogId);
//        File image = ffLogsDumper.capture(fflogId);
//
//        discordService.sendFile(message.getChannel(), image);
//      }
//    } catch (IOException | InterruptedException e) {
//      throw new RuntimeException(e);
//    }
//  }
//}
