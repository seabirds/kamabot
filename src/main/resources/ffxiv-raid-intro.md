__**Guides de boss :**__

__Arme Emeraude__
Texte : https://nawiel.live/castrum-marinum-arme-emeraude-ex/
Vidéo : https://www.youtube.com/watch?v=XC7x8BRWqXQ

__Arme Diamant__
Vidéo : https://www.youtube.com/watch?v=HbxdB8pjuB4&ab_channel=MikotoXI

__ Léviathan Irréel__
Texte : https://nawiel.live/le-briseur-des-marees-irreel/

__E9S__
Texte : https://nawiel.live/la-promesse-deden-nuee-sadique/
Vidéo :

- Mikoto : https://www.youtube.com/watch?v=7N2QxCyX80U
- Plava Station : https://www.youtube.com/watch?v=quprxO28SSc
- Mr Happy (ENGLISH YES SIR) : https://www.youtube.com/watch?v=WvIM5eEJk88

__**Guides & ressources de classes :**__

__Paladin__ (Keii) : https://discord.com/channels/277897135515762698/580300460179718146/810301537561215016
__Pistosabreur__ (Nilu) : https://discord.com/channels/277897135515762698/593562900539637760/744167086691057736

__Machiniste__ (Kamalen) : https://discord.com/channels/277897135515762698/592615206325059594/801989584421912619
__Danseur__ (Shira) : https://discord.com/channels/277897135515762698/593765592314413076/734524643981000745
__Chevalier Dragon__ (Balna) : https://discord.com/channels/277897135515762698/582430670006124564/721475477717516319
__Mage rouge__ (Roxas) : https://discord.com/channels/277897135515762698/592613258087628800/734193641253503028

__Mage blanc__ (Kaarth) : https://discord.com/channels/277897135515762698/593563541571764316/608871879079493652
__Erudit__ (Max) : https://discord.com/channels/277897135515762698/593563604729593856/748987785754443810

(serveur Discord The Balance, si pas accès, lien permanent : https://discord.gg/thebalanceffxiv )

Ce canal sera cleanup de temps en temps, pour ne pas perdre un message, il faut l'épingler.