package com.ethernity.kamabot.savage.mappers;

import com.ethernity.kamabot.savage.Savage;
import com.ethernity.kamabot.savage.SavageNeed;
import lombok.extern.slf4j.Slf4j;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class SavagePerFloorEmbedMapperTest
{
  @BeforeEach
  void setUp()
  {

  }

  @Test
  public void testEmbed()
  {
    SavageNeed mockSavageNeed = new SavageNeed();
    mockSavageNeed.setBody(true);
    mockSavageNeed.setWeapon(false);
    mockSavageNeed.setAgent(3);
    mockSavageNeed.setAgentObtained(1);
    mockSavageNeed.setFiber(2);
    mockSavageNeed.setFiberObtained(0);

    Map<String, SavageNeed> mockSavageNeedPlayerMap = new HashMap<>();
    mockSavageNeedPlayerMap.put("Player", mockSavageNeed);

    Savage mockSavage = new Savage();
    mockSavage.setNeeds(mockSavageNeedPlayerMap);

    EmbedBuilder embedBuilder = SavagePerFloorEmbedMapper.toPerFloorEmbed(mockSavage);

    log.info("Yo" + embedBuilder.toString());
  }
}